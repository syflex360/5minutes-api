<?php

namespace App;

use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\SocialAccount;

class SocialAccountService
{
    public function setOrGetUser(Provider $provider)
    {
        $providerUser = $provider->user();
        $providerName = class_basename($provider);

        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName,
                'token' =>  $providerUser->token,
                'expiresIn' => $providerUser->expiresIn,
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'avatar' => $providerUser->avatar,
                    'active' => true,
                    'password' => bcrypt('password'),
                ]);                
                
                //send notification to admin using the notifiable id of 1
                // $notify = User::where('id',$user->id)->first();
                // $body = $data['name'] .' ' .'Welcome to 5minutes';
                // $notify->notify(new UsersNotification($user,$body));

                //log this registration to the custom log 
                // \LogActivity::addToLog($data['name'].'registered at'.now());

                // send a  welcome email to this user that just registered. using queue
                // z$this->dispatch(new SendWelcomeEmail($user));


            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }

    }
}