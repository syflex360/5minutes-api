<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageSent extends Model
{
    Protected $fillable = ['to','from','message_id'];

    public function message()
        {
            return $this->belongsTo(Message::class, 'message_id');
        }

    public function sentTo(){
        return $this->belongsTo(User::class, 'to');
    }
}
