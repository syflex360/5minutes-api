<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $fillable = [
        'user_id','mentor_id','referrer_id','subscription_id','subscription_type_id','invoice_id','transaction_type',
        'transaction','currency_symbol','currency','amount_transacted','withdrawable_befor','amount_due','withdrawable_after','description'
        ];
    
        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function mentor()
        {
            return $this->belongsTo(User::class, 'mentor_id');
        }

        public function referrer()
        {
            return $this->belongsTo(User::class, 'referrer_id');
        }

        public function subscription()
        {
            return $this->belongsTo(Subscription::class, 'subscription_id');
        }
        public function exclusive_session()
        {
            return $this->belongsTo(ExclusiveSession::class, 'subscription_id');
        }

        public function subscription_type()
        {
            return $this->belongsTo(SubscriptionType::class, 'subscription_type_id');
        }
}
