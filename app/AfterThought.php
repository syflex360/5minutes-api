<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfterThought extends Model
{
    Protected $fillable = ['post_id', 'content'];

    public function post()
        {
            return $this->belongsTo(Post::class, 'post_id');
        }
}
