<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExclusiveSessionUser extends Model
{
    protected $fillable = [
        'user_id','exclusive_session_id','is_active','is_banned'
    ];

    public function exclusive_session()
    {
        return $this->belongsTo(ExclusiveSession::class, 'exclusive_session_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
