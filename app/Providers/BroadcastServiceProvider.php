<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Broadcast::routes(['middleware' => ['auth:api']]);

        // Broadcast::routes([ "prefix" => "api", "middleware" => "auth:api", ]);

        // Broadcast::channel('dm.{id}', function ($user, $id) {
        //     return true;// return (int) $user->id === (int) $id;
        // });
               
        require base_path('routes/channels.php');
    }
}
