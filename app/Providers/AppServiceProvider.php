<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use DB;
use Log;
use App\Post;
use App\Follower;
use Auth;
// use Laravel\Horizon\Horizon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Schema::defaultStringLength(191);
        DB::listen(function($query){
            Log::info(
                $query->sql,
                $query->bindings,
                $query->time
            );
        });
        Passport::routes();
        // Horizon::auth(function ($request) {
        //     // Always show admin if local development
        //     if (env('APP_ENV') == 'local') {
        //         return true;
        //     }
        // });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
