<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Facebook\Facebook;

class FacebookServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Facebook::class, function ($app) {
            $config = config('services.facebook');
            return new Facebook([
                'app_id' => env('FACEBOOK_CLIENT_ID', '1583464765041626'),
                'app_secret' => env('FACEBOOK_CLIENT_SECRET', 'f6484efdeabd9e8cbe74b60d3bf29985'),
                'default_graph_version' => 'v2.6',
            ]);
        });
    }
}
