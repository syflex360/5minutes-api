<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;
use Illuminate\Notifications\Notifiable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExclusiveSession extends Model
{
    use Notifiable, SoftDeletes, Sluggable, ElasticquentTrait;
    protected $fillable = [
        'user_id','name','slug','avatar','description','number_of_users','start_date','end_date'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function exclusive_session_pricing()
    {
        return $this->hasMany(ExclusiveSessionPricing::class);
    }
    
     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

}
