<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class follower extends Model
{
    Protected $fillable = ['user_id','mentor_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function mentor()
    {
        return $this->belongsTo(User::class, 'mentor_id');
    }

    
}
