<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Broadcast extends Model
{
    Protected $fillable = ['user_id','content','users'];
}
