<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    protected $fillable = [
        'user_id', 'name', 'email', 'phone','website','address','contact_name','contact_phone','contact_email'
    ];
}
