<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bookmark extends Model
{
    Protected $fillable = ['user_id','post_id'];
}
