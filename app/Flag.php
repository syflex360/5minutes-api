<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class flag extends Model
{
    Protected $fillable = ['user_id', 'flagable', 'flagable_id', 'reason'];
}
