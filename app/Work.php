<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    Protected $fillable = ['user_id','institution','position','from','to'];

    public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }
}
