<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
   Protected $fillable = ['user_id','type','institution','course','award','date'];
   public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }
}
