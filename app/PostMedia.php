<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostMedia extends Model
{
    protected $fillable = ['user_id','post_id', 'file', 'file_type', 'post_type'];

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
