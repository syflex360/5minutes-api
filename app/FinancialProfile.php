<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialProfile extends Model
{
    //
    Protected $fillable = ['user_id', 'account_name', 'account_number', 'bank_name','bank_ref_code','widthdrawal_code'];
}
