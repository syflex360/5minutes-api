<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\ForumConversation;

class NewForumMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    // protected $channel;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ForumConversation $data)
    {
        // $this->channel = $channel;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('forum.'.$this->data->forum_id);
    }

    // public function broadcastWith()
    // {
    //     return [
    //         'message' => $this->conversation->message,
    //         'user' => [
    //             'id' => $this->conversation->user->id,
    //             'name' => $this->conversation->user->name,
    //         ]
    //     ];
       
    // }
}
