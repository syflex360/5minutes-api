<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    protected $fillable = [
        'user_id','expirymonth','expiryyear','cardBIN','last4digits','brand','embedtoken','shortcode','expiry','life_time_token'
    ];
}
