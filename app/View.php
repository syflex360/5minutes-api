<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class view extends Model
{
    Protected $fillable = ['user_id', 'viewable_id','viewable_type'];
}
