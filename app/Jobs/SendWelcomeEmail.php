<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\User;

class SendWelcomeEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $user;
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Allow only 2 emails every 1 second
        Redis::throttle('Sent-Email-To-Mailtrap')->allow(2)->every(1)->then(function () {

            $user = $this->user;
		    Mail::to($user['email'])->send(new WelcomeMail($user));
            Log::info('Emailed order ' . $this->user->id);

        }, function () {
            // Could not obtain lock; this job will be re-queued
            return $this->release(2);
        });
        
    }
}
