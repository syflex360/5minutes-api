<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    Protected $fillable = ['user_id','institution','qualification','from','to'];

    public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }
}
