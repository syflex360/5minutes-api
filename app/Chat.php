<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Chat extends Model
{
    Protected $fillable = ['from','to','read','text'];
    protected $appends = ['ReadableDate'];
    public function getReadableDateAttribute(){
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']))->diffForHumans();
    }
    
}
