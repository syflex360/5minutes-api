<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    Protected $fillable = [
        'user_id','subscribe_to','subscription_type_id','type','transID','flwRef','invoice_number','currency_symbol',
        'currency','amount','charges','paymentURL','status','method','due_date','description'
    ];
        
    public function subscription(){
        return $this->belongsto(SubscriptionType::class, 'subscription_type_id');
    }
}
