<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Widthdrawable extends Model
{
    //
    protected $fillable = ['user_id','amount'];
}
