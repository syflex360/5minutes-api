<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Message extends Model
{
    Protected $fillable = ['from','to','read','text','status','read_at'];  
    
    protected $appends = ['ReadableDate'];
    public function getReadableDateAttribute(){
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']))->diffForHumans();
    }
    
    public function fromContact()
    {
        return $this->hasOne(User::class, 'id', 'from');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'to');
    }
}
