<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DirectMessage extends Model
{
    Protected $fillable = ['user_id','to_user_id','body','status'];
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
