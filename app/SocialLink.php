<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    protected $fillable = [
        'user_id', 'facebook', 'twitter', 'linkedin', 'instagram'
    ];
}
