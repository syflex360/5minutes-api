<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Interest;
use Auth;
use App\Mail\WelcomeMail;
use App\Notifications\UsersNotification;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notify = User::where('id','2')->first();
        $user = Auth::user();
        $body = 'you are back home';
        // $notify->notify(new UsersNotification($user,$body));
        return view('home');
    }

    public function forum(){
        $forums = auth()->user()->forums;

        $users = User::where('id', '<>', auth()->user()->id)->get();
        $user = auth()->user();

        return view('forum', ['forums' => $forums, 'users' => $users, 'user' => $user]);
    }
    
    public function feed()
    {      
        return view('feed');
    }

    public function myTestAddToLog()
    {
        \LogActivity::addToLog('My Testing Add To Log.');
        dd('log insert successfully.');
    }
   
    public function logActivity()
    {
        $logs = \LogActivity::logActivityLists();
        return view('logActivity',compact('logs'));
    }
}
