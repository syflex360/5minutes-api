<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\WelcomeMail;
use App\Jobs\SendWelcomeEmail;
use App\Notifications\UsersNotification;
use Illuminate\Support\Facades\Mail;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/feed';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => Hash::make($data['password']),
        ]);

        //send notification to admin using the notifiable id of 1
        $notify = User::where('id',$user->id)->first();
        $body = $data['name'] .' ' .'Welcome to 5minutes';
        $notify->notify(new UsersNotification($user,$body));

        //log this registration to the custom log 
        \LogActivity::addToLog($data['name'].'registered at'.now());
        \Log::info($data['name'].'registered at'.now());

        // send a  welcome email to this user that just registered
        // Mail::to($data['email'])->send(new WelcomeMail($user));
        // SendWelcomeEmail::dispatch($user)->onQueue('email');

        // send a  welcome email to this user that just registered using queue
        $this->dispatch(new SendWelcomeEmail($user));

        return $user;
    }
}
