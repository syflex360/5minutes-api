<?php

namespace App\Http\Controllers;

use App\Events\ForumCreated;
use App\Forum;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function store(Request $request)
    {
        $forum = Forum::create(['name' => request('name')]);

        $users = collect(request('users'));
        $users->push(auth()->user()->id);

        $forum->users()->attach($users);

        broadcast(new ForumCreated($forum))->toOthers();

        return $forum;
    }
}
