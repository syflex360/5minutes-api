<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Notifications\InvoicePaid;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Mail\ArticleSent;
use App\Post;
use App\Category;
use App\User;
use Carbon\Carbon;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // $invoice='paid invoice';
        // $users=User::get()->where('id', Auth::user()->id);  
        // foreach ($users as $user) {
        //     $user->notify(new InvoicePaid($invoice));
        // }
       
        $posts = Post::with('user.interest.category', 'comment', 'afterThought')->paginate(10);
        return view('dashboard/article/index', compact('posts'));
    }

    public function mail(){
        $post = ([
            'title' => 'hello',
            'post' => 'hey hello',
        ]);
      
        $recipient = 'syflex360@gmail.com';
        Mail::to($recipient)->send(new ArticleSent($post));
        return back();
    }

    public function draft()
    {   
        $posts = Post::where('status',0)->paginate(10);
        return view('dashboard/article/draft', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('dashboard/article/article', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $content=$request->get('contents');
 
        $dom = new \domdocument();
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
 
        $images = $dom->getelementsbytagname('img');
 
        foreach($images as $k => $img){
            $data = $img->getattribute('src');
 
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
 
            $data = base64_decode($data);
            $image_name= time().$k.'.png';
            $path = public_path() .'/'. $image_name;
 
            file_put_contents($path, $data);
 
            $img->removeattribute('src');
            $img->setattribute('src', $image_name);
        }
 
        $content = $dom->savehtml();

        $status = 0;
        if ($request->has('publish')){
            $status = 1;
        }elseif($request->has('draft')){
            $status = 0;
        }

        $is_premium = 0;
        if ($request->get('type')){
            $is_premium = 1;
        }else{
            $is_premium = 0;
        }

        if(($request->get('contentType')) === "video"){
           $file = $request->file('video');
           $ext =  $file->getClientOriginalExtension();
           $file_name = Auth::user()->id.'_'.str_slug($request->get('title')).'_'.str_replace('.tmp', '', $file->getFilename()).'.'.$ext;
           Storage::disk('video_post')->put($file_name, File::get($file));
           $content = $file_name;
        }
       
       $post = new Post([
            'user_id' => (Auth::user()->id),
            'category_id' => $request->get('interest'),
            'title' => $request->get('title'),
            'content' => $content,
            'content_type' => $request->get('contentType'),
            'status' => $status,
            'is_premium' => $is_premium,
            'published_at' => Carbon::parse(str_replace('-','',$request->get('date'))),
        ]);
        $post->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
