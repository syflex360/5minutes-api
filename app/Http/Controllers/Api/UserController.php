<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Validator;
Use App\User;
Use App\FinancialProfile;
Use App\SocialLink;
Use App\Organisation;
use App;

class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::with('interest','interest.category','followers','following','achievement','work','education','status','achievement')->where('id',Auth::user()->id)->get()->first();
        return response()->json($user);
    }

    public function getProfile(Request $request, $slug)
    {
        $profile = User::with('interest', 'interest.category','followers','following','achievement','education','work','status','achievement')->where('slug', $slug)->get()->first();
        return response()->json($profile);
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
        $user = User::where('slug',$slug)->withCount(
            [
                'posts as allPostCount' => function ($query) {
                     $query->where('status', 1);
                    },
                'posts as postThisMonthCount' => function ($query) {
                    $query->where('status', 1)
                        ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()]);
                   },
                'followers as allFollowersCount' => function ($query) {
                    $query->where('is_accepted', 1);
                   },
                'followers as followersThisMonthCount' => function ($query) {
                    $query->where('is_accepted', 1)   ;                     
                    // ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()]);
                   }
            ])->first(); 
        return response()->json(
            [
                'status' => 'successful',
                'data' => $user->load('status'),
            ], 
            $this-> successStatus
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $initDate = str_replace('T', ' ', $request->get('birthday'));
        $initDate = substr($initDate, 0, strpos($initDate, '+'));
        $date = date('Y-m-d h:i:s', strtotime($initDate));

        $user = User::where('id',$id)->update([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'birthday' => $date,
            'country_id' => $request->get('country_id'),
            'about' => $request->get('about'),
        ]);

        $image = $request->input('avatar'); // image base64 encoded

        if($image){
            $user = Auth::user();
            preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $imageName = $user->slug.'_' . time() . '.' . $image_extension[1]; //generating unique file name;

            // upload users profile picture to drive
            $environment = App::environment();
            $disk = $environment == 'local' ? 'local' : 's3';
            Storage::disk($disk)->put('profile/'.$user->id.'/'.$imageName, base64_decode($image), 'public');
            
            // update users profile picture in database
            User::where('id', $id)->update(['avatar' => $imageName]);
        }
        
        
        $finicials = FinancialProfile::updateOrCreate(['user_id' => $id],[
            'user_id' => $id,
            'account_name' => $request->get('account_name'),
            'account_number' => $request->get('account_number'),
            'bank_name' => $request->get('bank_name'),
            'bank_ref_code' => $request->get('bank_ref_code'),
            'widthdrawal_code' => $request->get('widthdrawal_code')
        ]);

        $social = SocialLink::updateOrCreate(['user_id' => $id],[
            'user_id' => $id,
            'facebook' => $request->get('facebook'),
            'twitter' => $request->get('twitter'),
            'linkedin' => $request->get('linkedin'),
            'instagram' => $request->get('instagram'),
        ]);

        $social = Organisation::updateOrCreate(['user_id' => $id],[
            'user_id' => $id,
            'name' => $request->get('org_name'),
            'email' => $request->get('org_email'),
            'phone' => $request->get('org_phone'),
            'website' => $request->get('org_website'),
            'address' => $request->get('org_address'),
        ]);


        return response()->json([
            'status' => 'successful',
            'date' => $user,
            'msg'=>'profile updated successfully'
        ]);
    }

    public function updateProfile(Request $request)
    {
        $user = User::where('id',Auth::user()->id)
            ->update([
                'phone' => $request->get('phone'),
                'birthday' => $request->get('birthday'),
                'country_id' => $request->get('country_id'),
                'about' => $request->get('about'),
        ]);            

        return response()->json(['status' => 'success','msg'=>'profile updated successfully']);
    }

    public function become_referrer(Request $request){
        $referrer_code = null;
        // Delimit by multiple spaces, hyphen, underscore, comma
        $words = preg_split("/[\s,_-]+/", auth()->user()->name);
        $acronym = "";
        foreach ($words as $w) {
        $acronym .= $w[0];
        }
        $number = mt_rand(100000, 999999);
        $referrer_code = (strtoupper($acronym).$number);
        $data =  User::find(auth()->user()->id)->update(['is_referrer' => true, 'referrer_code' => $referrer_code]);
        return response()->json($data);
    }

    public function upload_avatar(Request $request){
        $image = $request->avatar; // image base64 encoded
        $user = Auth::user();
        preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
        $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
        $image = str_replace(' ', '+', $image);
        $imageName = $user->slug.'_' . time() . '.' . $image_extension[1]; //generating unique file name;

        // upload users profile picture to drive
        $environment = App::environment();
        $disk = $environment == 'local' ? 'local' : 's3';
        Storage::disk($disk)->put('profile/'.$user->id.'/'.$imageName, base64_decode($image), 'public');
        
        // update users profile picture in database
        $avatar = User::where('id', $user->id)->update(['avatar' => $imageName]);

        return $avatar;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
