<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Note;
use Validator;
use Auth;

class NoteController extends Controller
{
     public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $messages = [
            'title.required'    => 'Enter Note Title!',
            'note.required' => 'Enter Note Content!'
        ];

        $validator = Validator::make($request->all(), [ 
            'title' => 'required', 
            'note' => 'required',
        ], $messages);

        
        if ($validator->fails()) {             
            return response()->json([
                'status' => 'error',
                'error' => $validator->errors(),
                'message' => 'Error Saving Note',
            ], 401);           
        }else{
            $note = Note::create([
                'user_id' => Auth::user()->id,
                'title' => $request->get('title'),
                'note' => $request->get('note'),
            ]);
            return response()->json(
                [
                    'status' => 'successful',
                    'data' => $note,
                ], 
                $this-> successStatus
            ); 
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
