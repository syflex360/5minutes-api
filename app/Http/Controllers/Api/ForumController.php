<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Forum;
use App\ForumUser;
use Auth;

class ForumController extends Controller
{
    public $successStatus = 201;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forums = Forum::orderBy('name')->get();
        return response()->json(
            [
                'status' => 'successful',
                'data' => $forums,
            ], 
            $this-> successStatus
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $forum = Forum::create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'description' => $request->description,
        ]);

        $forum_id = $forum->id;

        // prepare all selected users for batch insert;
        foreach($request->users as  $user_id){
            $insertData[] = array(
                'forum_id' => $forum_id,
                'user_id' => $user_id,
            );
        }

        // add forum owner to the forum users list
        $insertData[] = array(
            'forum_id' => $forum_id,
            'user_id' => Auth::user()->id,
        );

        // batch insert forum users
        $forum_users = ForumUser::insert($insertData);

        // return success message 
        return response()->json(
            [
                'status' => 'successful',
                'data' => $forum,
            ], 
            $this-> successStatus
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
