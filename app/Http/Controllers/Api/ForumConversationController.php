<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Events\MessagePushed;
use App\Events\NewForumMessage;
use App\Events\NewMessage;
use App\ForumConversation;
use Auth;
use DB;

class ForumConversationController extends Controller
{
    public $successStatus = 201;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //insert forum conversations 
        $message = ForumConversation::create([
            'message' => $request->get('message'),
            'user_id' => Auth::user()->id,
            'forum_id' => $request->get('forum_id'),            
        ]);   
      
        // Log::info('broadcast query startd');
        $message = ForumConversation::where('id',$message->id)->with('user:id,name,avatar')->first();
        Log::info('broadcast query startd');
        // broadcast event. Will be broadcasted over pusher.
        broadcast(new NewForumMessage($message))->toOthers();
        // broadcast(new NewMessage($message));
        // Log::info('broadcast query success');
        
        return response()->json(
            [
                'status' => 'successful',
                'data' => $message,
            ], 
            $this-> successStatus
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ForumConversation::where('forum_id', $id)->with('user:id,name,avatar')->get();
        return response()->json(
            [
                'status' => 'successful',
                'data' => $data,
            ], 
            $this-> successStatus
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
