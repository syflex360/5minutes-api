<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Post;
use App\Comment;
use App\Follower;
use App\Bookmark;
use App\Like;
use App\Interest;
use App\Pin;

class FollowController extends Controller
{
    /**
     * Toggle the follow state for the given content item.
     * 
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response 
     */
    public function toggleFollow(Request $request)
    {
        if ($request->type === 'follow'){
            $checkStatus = Follower::where('user_id', '=', (Auth::user()->id))->where('mentor_id', $request->id)->count();
            if(!$checkStatus){
                $follower = new Follower([
                    'user_id' => Auth::user()->id,
                    'mentor_id' => $request->id,            
                ]);
                $follower->save();
            }else{
                $follower =  Follower::where('user_id',(Auth::user()->id))->where('mentor_id', $request->id);                   
                $follower->delete(); 
            }
            return response()->json(['success' => true], 200);
        }elseif($request->type === 'bookmark'){
            $checkStatus = Bookmark::where('user_id', '=', (Auth::user()->id))->where('post_id', $request->id)->count();
            if(!$checkStatus){
                $follower = new Bookmark([
                    'user_id' => Auth::user()->id,
                    'post_id' => $request->id,            
                ]);
                $follower->save();
            }else{
                $follower =  Bookmark::where('user_id',(Auth::user()->id))->where('post_id', $request->id);                   
                $follower->delete(); 
            }
            return response()->json(['success' => true], 200);
        }elseif($request->type === 'pin'){
            $checkStatus = Bookmark::where('user_id', '=', (Auth::user()->id))->where('post_id', $request->id)->count();
            if(!$checkStatus){
                $follower = new Bookmark([
                    'user_id' => Auth::user()->id,
                    'post_id' => $request->id,            
                ]);
                $follower->save();
            }else{
                $follower =  Bookmark::where('user_id',(Auth::user()->id))->where('post_id', $request->id);                   
                $follower->delete(); 
            }
            return response()->json(['success' => true], 200);
        }elseif($request->type === 'interest'){
            $checkStatus = Interest::where('user_id', '=', (Auth::user()->id))->where('category_id', $request->id)->count();
            if(!$checkStatus){
                $follower = new Interest([
                    'user_id' => Auth::user()->id,
                    'category_id' => $request->id,            
                ]);
                $follower->save();
            }else{
                $follower =  Interest::where('user_id',(Auth::user()->id))->where('category_id', $request->id);                   
                $follower->delete(); 
            }
            return response()->json(['success' => true], 200);
        }elseif($request->type === 'post' || $request->type === 'comment'){
            $checkStatus = Like::where('user_id', '=', (Auth::user()->id))->where('likeable_id', $request->id)->count();
            if(!$checkStatus){
                $follower = new Like([
                    'user_id' => Auth::user()->id,
                    'likeable_id' => $request->id, 
                    'likeable_type' => $request->type,           
                ]);
                $follower->save();
                if($request->type === 'post'){
                    Post::where('id',$request->id)->increment('likes',1);
                }elseif($request->type === 'post'){
                    Comment::where('id', $request->id)->increment('likes',1);
                }
            }else{
                $follower =  Like::where('user_id',(Auth::user()->id))->where('likeable_id', $request->id);                   
                $follower->delete();               
                if($request->type === 'post'){
                    Post::where('id',$request->id)->decrement('likes',1);
                }elseif($request->type === 'post'){
                    Comment::where('id',$request->id)->decrement('likes',1);
                }
            }
            return response()->json(['success' => true], 200);
        }
        
    }

    /**
     * Check if the user is already following the given content item
     * 
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function following(Request $request)
    {        
        if ($request->type === 'follow'){
        $item =  (bool) Follower::where('user_id', Auth::user()->id)
                        ->where('mentor_id', $request->id)
                        ->first();
        return response()->json($item);
        }elseif($request->type === 'bookmark'){
            $item =  (bool) Bookmark::where('user_id', Auth::user()->id)
            ->where('post_id', $request->id)
            ->first();
        return response()->json($item);
        }elseif($request->type === 'interest'){
            $item =  (bool) Interest::where('user_id', Auth::user()->id)
            ->where('category_id', $request->id)
            ->first();
        return response()->json($item);
        }elseif($request->type === 'post' || $request->type === 'comment'){
            $item =  (bool) Like::where('user_id', Auth::user()->id)
            ->where('likeable_id', $request->id)
            ->first();
        return response()->json($item);
        }elseif($request->type === 'friendLikes'){
            // $item =  (bool) Like::where('user_id', Auth::user()->id)
            // ->where('post_id', $request->id)
            // ->first();
        return response()->json($item);
        }

    }

     /**
     * get all followings
     * 
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function followings($slug)
    {        
        $user = User::where('slug',$slug)->first();
        $followings = Follower::where('user_id', $user->id)->with('mentor:id,name,avatar','user.status')->get();
        return response()->json($followings);
    }

    public function followers($slug)
    {        
        $user = User::where('slug',$slug)->first();
        $followers = Follower::where('mentor_id', $user->id)->with('user:id,name,avatar','user.status')->get();
        return response()->json($followers);
    }

}
