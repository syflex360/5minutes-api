<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interest;
use App\Category;
use App\Post;
use App\User;

class CategoryController extends Controller
{
    public $successStatus = 201;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Category::orderBy('name','ASC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function countArticles($id){
       return Post::where('category_id', $id)->count();
    }

    public function countMentors($id){
        $data = Interest::where('category_id', $id)->get();
        
        foreach ($data as $item) {
           $ids[] = $item['user_id'];
        }

        if($ids){ 
            $users = User::whereIn('id', $ids)->where('is_mentor', 1)->with('status')->get(); 
            return response()->json(
                [
                    'status' => 'successful',
                    'data' => $users,
                ], $this-> successStatus
            ); 
        }else{
            return response()->json(
                [
                    'status' => 'no data',
                ], $this-> successStatus
            ); 
        }

    }
}
