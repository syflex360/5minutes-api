<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;
use App\Invoice;
use App\SubscriptionType;
use App\Transaction;
use App\Widthdrawable;
use Carbon\Carbon;
use App\User;
use Auth;
use App;

class SubscriptionController extends Controller
{
    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscription = Subscription::where('user_id', Auth::user()->id)
        ->with('mentor:id,name,avatar,status','subscriptionType')
        ->get();
        
       return response()->json(
        [
            'status' => 'successful',
            'data' => $subscription,
            'message' => 'subscription featched successfuly',
        ], 
        $this-> successStatus
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get invoice and subscriptions cateory details
        $invoice = Invoice::where('id', $request->get('invoice_id'))->first();

        $subscription_type = SubscriptionType::where('id', $invoice->subscription_type_id)->first();
        
        // insert into Subscription table 
        $subscription = Subscription::create([
            'user_id' => Auth::user()->id,
            'mentor_id' => $invoice->subscribe_to,
            'subscription_type_id' => $invoice->subscription_type_id,
            'invoice_id' => $invoice->id,
            'start' => Carbon::now(),
            'end' =>  Carbon::now()->addDays($subscription_type->days),
        ]);

        // check if mentor had transactions before else set the date to start counting refferers 5% expiry date
        $mentor = User::where('id', $invoice->subscribe_to)->first();
        if(!$mentor->referrer_start_date){
            User::where('id',$mentor->id)->update([
                'referrer_start_date' =>  Carbon::now(),
                'referrer_end_date' => Carbon::now()->addYear(),
            ]); 
        }

        $amount_due = (75 / 100) * ($invoice->amount);

        // find mentors widthdrawable or create new (get mentors withdrwable details)
        $mentor_widthdrawable = Widthdrawable::firstOrCreate(array('user_id' =>  $mentor->id));

        // add amount due to mentors withdrawable if USD or Local Currency
        $widthdrawable_location = ($invoice->currency == 'USD') ? 'USD' : 'local' ;
        Widthdrawable::where('id', $mentor_widthdrawable->id)
            ->update([
                $widthdrawable_location => $amount_due + $mentor_widthdrawable->$widthdrawable_location,
            ]); 
        

        // insert transaction for the mentors 75%
        $transaction = Transaction::create([
            'user_id' => Auth::user()->id,
            'mentor_id' => $invoice->subscribe_to,
            'subscription_id' => $subscription->id,
            'subscription_type_id' => $invoice->subscription_type_id,
            'invoice_id' =>  $invoice->id,
            'transaction_type' => $invoice->type,
            'transaction' => 'credit',
            'currency_symbol' => $invoice->currency_symbol,
            'currency' => $invoice->currency,
            'amount_transacted' => $invoice->amount,
            'withdrawable_befor' => $mentor_widthdrawable->$widthdrawable_location,
            'amount_due' => $amount_due,
            'withdrawable_after' => ($mentor_widthdrawable->$widthdrawable_location) + ($amount_due),
            'description' =>  '',
        ]);
        
        // if mentors referrer is still valid else add the 5% to 5minutes account
        if($mentor->referrer_start_date && $mentor->referrer_end_date >= Carbon::now()){
            // get referrer details
            $referrer = User::where('referrer_code', $mentor->referrer)->first();

            // find referrer widthdrawable or create new (get referrer withdrwable details)
            $referrer_widthdrawable = Widthdrawable::firstOrCreate(array('user_id' => $referrer->id));

            $amount_due = (5 / 100) * ($invoice->amount);

            // update withdrawables
            $widthdrawable_location = ($invoice->currency == 'USD') ? 'USD' : 'local';
            Widthdrawable::where('id', $referrer_widthdrawable->id)
            ->update([
                $widthdrawable_location => $amount_due + $referrer_widthdrawable->$widthdrawable_location,
            ]); 

            $transaction = Transaction::create([
                'user_id' => Auth::user()->id,
                'mentor_id' => $invoice->subscribe_to,
                'referrer_id' => $referrer->id,
                'subscription_id' => $subscription->id,
                'subscription_type_id' => $invoice->subscription_type_id,
                'invoice_id' =>  $invoice->id,
                'transaction_type' => $invoice->type,
                'transaction' => 'credit',
                'currency_symbol' => $invoice->currency_symbol,
                'currency' => $invoice->currency,
                'amount_transacted' =>  $invoice->amount,
                'withdrawable_befor' => $referrer_widthdrawable->$widthdrawable_location,
                'amount_due' => $amount_due,
                'withdrawable_after' => ($referrer_widthdrawable->$widthdrawable_location) + ($amount_due ),
                'description' =>  '',
            ]);            
        }else{
            // get 5minutes referrer details
            $five_minutes_referrer = User::where('id', 1)->first();

            // find 5minutes referrer widthdrawable or create new (get 5minutes referrer withdrwable details)
            $five_minutes_referrer_widthdrawable = Widthdrawable::firstOrCreate(array('user_id' =>  $five_minutes_referrer->id));

            $amount_due = (5 / 100) * ($invoice->amount);

            // update widthdrawables
            $widthdrawable_location = ($invoice->currency == 'USD') ? 'USD' : 'local';
            Widthdrawable::where('id', $five_minutes_referrer_widthdrawable->id)
            ->update([
                $widthdrawable_location => $amount_due + $five_minutes_referrer_widthdrawable->$widthdrawable_location,
            ]); 

            $transaction = Transaction::create([
                'user_id' => Auth::user()->id,
                'mentor_id' => $invoice->subscribe_to,
                'referrer_id' => $five_minutes_referrer->id,
                'subscription_id' => $subscription->id,
                'subscription_type_id' => $invoice->subscription_type_id,
                'invoice_id' =>  $invoice->id,
                'transaction_type' =>  $invoice->type,
                'transaction' => 'credit',
                'currency_symbol' => $invoice->currency_symbol,
                'currency' => $invoice->currency,
                'amount_transacted' =>  $invoice->amount,
                'withdrawable_befor' => $five_minutes_referrer_widthdrawable->$widthdrawable_location,
                'amount_due' => $amount_due,
                'withdrawable_after' => ($five_minutes_referrer_widthdrawable->$widthdrawable_location) + ($amount_due),
                'description' =>  '',
            ]);           
        }


        return response()->json(
            [
                'status' => 'successful',
                'data' => $invoice,
                'message' => 'Payment successfull',
            ], 
            $this-> successStatus
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {       
        // $user = User::where('slug',$id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
