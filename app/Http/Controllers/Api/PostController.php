<?php

namespace App\Http\Controllers\Api;

use App\Events\StatusWasUpdated;
use App\Http\Controllers\Controller;
use App\Traits\ApiJsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\ExclusiveSession;
use Carbon\Carbon;
use App\Post;
use App\User;
use App\View;
use App\Interest;
use App\Category;
use App\Follower;

class PostController extends Controller
{
    public $successStatus = 200;
    
    use ApiJsonResponse;
    // use Notifiable;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        try{
            $friends = Follower::where('user_id',auth()->user()->id)->pluck('mentor_id');
            $posts = Post::with('category','postMedia','user','user.status','like.user:id,name,avatar')                    
                    ->where('is_exclusive', 0)
                    ->whereDate('published_at', '<=', Carbon::now())
                    ->whereIn('user_id', $friends)
                    ->orWhere('user_id', auth()->user()->id)
                    ->withCount('comments','afterThought')
                    ->orderBy('published_at', 'DESC')       
                    ->paginate(50);
            
            return response()->json(
                [
                    'status' => 'successful',                    
                    'message' => "Posts fetched",
                    'data' => $posts,
                ],                 
                $this-> successStatus
            );

        }catch(\Exception $exception){
            return response()->json($exception->getMessage());           
        }
        
    }

    
    public function categoryPost($name)
    { 
        $category = Category::where('name',$name)->first();
        try{
            $posts = Post::with('category','postMedia','user','user.status','like.user:id,name,avatar')
                    ->where('category_id', $category->id)                
                    ->where('is_exclusive', 0)
                    ->whereDate('published_at', '<=', Carbon::now())
                    ->withCount('comments','afterThought')
                    ->orderBy('published_at', 'DESC')          
                    ->paginate(50);
            return response()->json($posts);
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
       try{
           
            $id = Auth::user()->id;
            $status = '';

            if($request->get('published_at')) {
                $initDate = str_replace('T', ' ', $request->get('published_at'));
                $initDate = substr($initDate, 0, strpos($initDate, '+'));
                $date = date('Y-m-d h:i:s', strtotime($initDate));
                $status = ($date > Carbon::now()) ? 'queue' : null;
            }

            if ($request->get('exclusive_session')) {
                $sesion = ExclusiveSession::where('slug', $request->get('exclusive_session'))->first();
            }                

            $input = $request->all(); 
            $input['user_id'] = $id;
            $input['content'] = $request->get('post');
            $input['published_at'] = $request->get('published_at') ? $date : Carbon::now();
            $input['exclusive_session_id'] = $request->get('exclusive_session') ? $sesion->id : null;
            $new_post = Post::create($input);

            $posts = Post::where('id', $new_post->id)->with('category','postMedia','user','user.status','like.user:id,name,avatar')
                    ->withCount('comments','afterThought')
                    ->first();               
                            
            return response()->json(
                [
                    'status' => 'successful',  
                    'queued' => $status,                 
                    'message' => "Posts saved",
                    'data' => $posts,
                ],                 
                $this-> successStatus
            );
        // }
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $user = User::where('slug',$slug)->first();
        try{
            $posts = Post::where('user_id',$user->id)->with('category','postMedia','user','user.status','like.user:id,name,avatar')
                    ->withCount('comments','afterThought')
                    ->orderBy('published_at', 'DESC')                   
                    ->paginate(50);
            
            // $posts->each(function($post) {
            //     $post['comments'] = $post->getThreadedComments();
            // });

            return response()->json(
                [
                    'status' => 'successful',                    
                    'message' => "Posts fetched",
                    'data' => $posts,
                ],                 
                $this-> successStatus
            );
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());           
        }
    }


    public function get_inserted_post($post_id)
    {        
        try{
            $posts = Post::where('id', $post_id)->with('category','postMedia','user','user.status','like.user:id,name,avatar')
                    ->withCount('comments','afterThought')
                    ->first();               
                            
            return response()->json(
                [
                    'status' => 'successful',              
                    'message' => "Posts saved",
                    'data' => $posts,
                ],                 
                $this-> successStatus
            );
        }catch(\Exception $exception){
            return response()->json($exception->getMessage());           
        }
    }
      
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //        
        $initDate = str_replace('T', ' ', $request->get('published_at'));
        $initDate = substr($initDate, 0, strpos($initDate, '+'));
        $date = date('Y-m-d h:i:s', strtotime($initDate));
        
        
        $update_post = Post::find($id);
        $update_post->title = $request->get('title');
        $update_post->content = $request->get('post');
        $update_post->allow_comment = $request->get('allow_comment');
        $update_post->content_type = $request->get('content_type');
        $update_post->is_signed = $request->get('is_signed');
        $update_post->is_premium = $request->get('is_premium');
        // $update_post->published_at = $date;
        $update_post->save();

        $posts = Post::where('id', $update_post->id)->with('category','postMedia','user','afterThought','user.status','like.user:id,name,avatar')
                ->withCount('comments')->first();
    
        // $posts->each(function($post) {
        //     $post['comments'] = $post->getThreadedComments();
        // });

        return response()->json(
            [
                [
                    'status' => 'successful',
                    'data' => $posts,
                    'message' => 'Updated successfully'
                ], 
                $this-> successStatus
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);
        if($post){
          $post->delete();
          return response()->json(
            [
                'status' => 'successful',
                'data' => $post,
                'message' => 'Record Deleted successfully'
            ], $this-> successStatus );
        } else {
          return response()->json(
            [
            'statur'=>'error',
            'message'=>'Error deleting record'
            ], 404 );
        }
    }
    
    public function postViews(Request $request){   
        if($request->type == 'post'){            
            $id = Post::where('slug', $request->vieable_id)->first();
            $view = View::where('viewable_id', $id->id)->where('user_id',Auth::user()->id)->first();
            if(!$view){
                Post::where('id', $id->id)->increment('views',1);
                $view = new View([
                    'user_id' => Auth::user()->id,
                    'viewable_id' => $id->id,
                    'viewable_type' => $request->type,
                    ]);
                $view->save();
            }else{
                
            }
            
        }
       
    }

    
}
