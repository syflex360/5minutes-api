<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Achievement;
use Auth;

class AchievementController extends Controller
{
    public $successStatus = 201;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $achievement = Achievement::where('user_id', Auth::user()->id)->orderBy('id','DESC')->getQuery()->get();
        return response()->json(
            [
                'status' => 'successful',
                'data' => $achievement,
                'message' => 'Achievement fetched successfully'
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // format date from (javascript date to php datetime)
        $initDateFrom = str_replace('T', ' ', $request->get('date_from'));
        $initDateFrom = substr($initDateFrom, 0, strpos($initDateFrom, '+'));
        $dateFrom = date('Y-m-d h:i:s', strtotime($initDateFrom));
        // format date to (javascript date to php datetime)
        $initDateTo = str_replace('T', ' ', $request->get('date_to'));
        $initDateTo = substr($initDateTo, 0, strpos($initDateTo, '+'));
        $dateTo = date('Y-m-d h:i:s', strtotime($initDateTo));

        $achievement = Achievement::create([
            'user_id' => Auth::user()->id,
            'type' => $request->get('type'),
            'institution' => $request->get('institution'),
            'course' => $request->get('course'),
            'award' => $request->get('award'),
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
        ]);

        return response()->json(
            [
                'status' => 'successful',
                'data' => $achievement,
                'message' => 'Achievement saved and fetched successfully'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $achievement = Achievement::where('user_id',$id)->get(); 
        return response()->json(
            [
                'status' => 'successful',
                'data' => $achievement,
                'message' => 'Achievement fetched successfully'
            ], 
            $this-> successStatus
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Achievement::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        if($request->get('type') === 'default'){
            Achievement::where('user_id', Auth::user()->id)->update(['is_default' => 0]);
            Achievement::where('id', $id)->update(array('is_default' => 1));
        }else{
            // format date from (javascript date to php datetime)
            $initDateFrom = str_replace('T', ' ', $request->get('date_from'));
            $initDateFrom = substr($initDateFrom, 0, strpos($initDateFrom, '+'));
            $dateFrom = date('Y-m-d h:i:s', strtotime($initDateFrom));
            // format date to (javascript date to php datetime)
            $initDateTo = str_replace('T', ' ', $request->get('date_to'));
            $initDateTo = substr($initDateTo, 0, strpos($initDateTo, '+'));
            $dateTo = date('Y-m-d h:i:s', strtotime($initDateTo));  

            $achievement = Achievement::find($id);
            $achievement->institution = $request->get('institution');
            $achievement->course = $request->get('course');
            $achievement->award = $request->get('award');
            $achievement->date_from = $dateFrom;
            $achievement->date_to = $dateTo;
            $achievement->save();
            
            return response()->json(
                [
                    [
                        'status' => 'successful',
                        'data' => $achievement,
                        'message' => 'Updated successfully'
                    ], 
                    $this-> successStatus
                ]);
            
        }
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $achievement = Achievement::find($id);
        if($achievement){
          $achievement->delete();
          return response()->json(
            [
                'status' => 'successful',
                'data' => $achievement,
                'message' => 'Record Deleted successfully'
            ]);
        } else {
          return response()->json(
            [
            'statur'=>'error',
            'message'=>'Error deleting record'
            ]);
        }
    }
}
