<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Follower;
use App\Broadcast;
use Auth;

class BroadcastController extends Controller
{
    public $successStatus = 201;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $create = Broadcast::create([
            'user_id' => (Auth::user()->id),
            'content' => $request->get('post'),
            'users' => $request->get('category'),
        ]); 

        $users;
        
        if($request->category == 'all'){
            $users = Follower::where('mentor_id', Auth::user()->id)->orWhere('user_id', Auth::user()->id)->get();
        }elseif($request->category == 'subscribers'){
            $users = Follower::where('mentor_id', Auth::user()->id)->orWhere('user_id', Auth::user()->id)->get();
        }elseif($request->category == 'none-subscribers'){
            $users = Follower::where('mentor_id', Auth::user()->id)->orWhere('user_id', Auth::user()->id)->get();
        }
        
        return response()->json(
            [
                'status' => 'successful',
                'data' => $create,
            ], 
            $this-> successStatus
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
