<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DirectMessage;
use Auth;
use App\Events\DirectMessagePush;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class DirectMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = Auth::user()->id;
        if($request->get('type') == 'users'){
            return DirectMessage::with(['user'=>function($query){
                $query->select('id','name','avatar');
            }])
                    ->select('user_id')
                    ->where('to_user_id', $user_id)
                    ->groupBy('user_id')
                    ->distinct()
                    ->get();            
        }elseif($request->get('type') == 'unread message'){
            return DirectMessage::with(['user'=>function($query){
                $query->select('id','name','avatar');
            }])
                    ->where('to_user_id', $user_id)
                    ->where('status', 0)
                    ->get();
        }       
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $messages = DirectMessage::with(['user'])->get();

        return response()->json($messages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [            
            'body' => 'required',
          ]);
  
          //insert to direct message db
          $create = DirectMessage::create([
              'to_user_id' => $request->get('id'),
              'user_id' => Auth::user()->id,
              'body' => $request->get('body'),
          ]);

          //get the inserted info using the last inserted id
          $message = DirectMessage::where('id', $create->id)->with(['user'=>function($query){
            $query->select('id','name','avatar');
            }])->first();  

        //pass information to the event for broadcast
        Log::info('broadcasting dm');
            event(new DirectMessagePush($message, $create->to_user_id));           
        Log::info('dm broadcasted');

          return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = Auth::user()->id;
        $to_id = $id;
        return DirectMessage::with(['user'=>function($query){
            $query->select('id','name','avatar');
        }])
        ->where(function ($query) use ($user_id, $to_id){
            $query
            ->where('user_id', $to_id)
            ->where('to_user_id', $user_id);
        })->orWhere(function ($query) use ($user_id, $to_id){
            $query
            ->Where('user_id', $user_id)
            ->where('to_user_id', $to_id);
        })
        ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
