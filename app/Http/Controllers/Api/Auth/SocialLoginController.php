<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\SocialAccountService;

class SocialLoginController extends Controller
{
        public $successStatus = 200;


    public function redirectToSocial($social)
    {
        return Socialite::driver($social)->stateless()->redirect()->getTargetUrl();
    }

    function handleSocialCallback(SocialAccountService $service, $social)
    {        
        
        // return redirect('http://localhost:8080/login/'.'user');

         try {
            $user = $service->setOrGetUser(Socialite::driver($social)->stateless());
            // \Log::info($user);            
            // auth()->login($user);
            // return redirect('/feed');
            $token = $user->createToken('5minutes')-> accessToken;           
            return response()->json(
                [
                    'status' => 'successful',
                    'data' => $user,
                    'token' => "Bearer"." ".$token,
                ],                 
                $this-> successStatus
            ); 
        } catch (\Exception $e) {
            // \Log::error('social login error: '.$e);
            return $e;
        }



    }
        
}
