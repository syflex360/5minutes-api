<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\SignupActivate;
use App\Mail\WelcomeMail;
use App\Mail\MentorsPreconfirmationMessage;
use App\Notifications\WelcomeNotification;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Avatar;
use Storage;
use League\Flysystem\Filesystem;
use App\SocialLink;
use App\Organisation;
use App;

class UserController extends Controller
{
    
    public $successStatus = 201;

/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            
            if(Auth::user()->active){
                $id = Auth::user()->id;
                $user = Auth::user();
                $token = "Bearer"." ".$user->createToken('5minutes')-> accessToken;   
                return $this->get_user_details($id, $token);
            }else{
                return response()->json(
                    [
                        'status' => 'inactive',
                        'data' =>  $user = Auth::user(),
                        'error' => 'user note activated',
                        'message' => 'Please activte our account to loggin',
                    ], $this-> successStatus );
            }
            
        }else{ 
            return response()->json(
                [
                    'status' => 'error',
                    'error'=>'Unauthorised',
                    'message'=>'user',
                ], 401); 
        } 
    }


/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request){ 
        
        $messages = [
            'name.required'    => 'Enter full name!',
            'email.required' => 'Enter an e-mail address!',
            'email' => 'E-mail address exist!',
            'password.required'    => 'Password is required',
            'password_confirmation' => 'The :password and :password_confirmation must match.'
        ];

        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required', 
            'password_confirmation' => 'required|same:password', 
        ], $messages);

        $user = User::where('email', $request->get('email'))->first();
        
        if($user) { 
            return response()->json([
                'status' => 'exist',
                'error' => 'user exist',
                'message' => 'User already exist. please login',
            ], $this-> successStatus );   
        }elseif($validator->fails()){
            return response()->json([
                'status' => 'error',
                'error' => $validator->errors(),
                'message' => 'an error occured. Check that your form fields are correct and try again',
            ], $this-> successStatus);
        }else{

            //generate referrer code if user is registringg as referrer
            $referrer_code = null;
            if($request->is_referrer == true){
                // Delimit by multiple spaces, hyphen, underscore, comma
                $words = preg_split("/[\s,_-]+/", $request->name);
                $acronym = "";
                foreach ($words as $w) {
                $acronym .= $w[0];
                }
                $number = mt_rand(100000, 999999);
                $referrer_code = (strtoupper($acronym).$number);
            }

            // insert new record
            $input = $request->all(); 
            $input['password'] = bcrypt($input['password']);
            $input['activation_token'] = str_random(60);
            $input['referrer_code'] = $referrer_code;
            $user = User::create($input);

            // generate users avatar
            $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
            
            // local and s3
            $environment = App::environment();
            $disk = $environment == 'local' ? 'local' : 's3';
            Storage::disk($disk)->put('profile/'.$user->id.'/avatar.png', (string) $avatar, 'public');

            if($request->get('type') == 'mentor'){
                $social = SocialLink::create([
                    'user_id' => $user->id,
                    'facebook' => $request->get('facebook'),
                    'twitter' => $request->get('twitter'),
                    'linkedin' => $request->get('linkedin'),
                ]);
                //send mentors pre-confirmation email
                if($environment != 'local'){
                    Mail::to($user)->send(new MentorsPreconfirmationMessage($user));                    
                }   
            }elseif($request->get('type') == 'organisation'){
                $social = Organisation::create([
                    'user_id' => $user->id,
                    'name' => $request->get('name'),
                    'email' => $request->get('org_email'),
                    'phone' => $request->get('phone'),
                    'website' => $request->get('org_website'),
                    'address' => $request->get('org_address'),
                    'contact_name' => $request->get('contact_name'),
                    'contact_phone' => $request->get('contact_phone'),
                ]);
                $social = SocialLink::create([
                    'user_id' => $user->id,
                    'facebook' => $request->get('facebook'),
                    'twitter' => $request->get('twitter'),
                    'linkedin' => $request->get('linkedin'),
                ]);
                if($environment != 'local'){
                    Mail::to($user)->send(new MentorsPreconfirmationMessage($user));
                }  
            }else{
                if($environment != 'local'){
                    $user->notify(new SignupActivate($user));
                }                
            }     
            
            $user->notify(new WelcomeNotification($user));

            return response()->json(
                [
                    'status' => 'successful',
                    'data' => $user,
                    'message' => 'User created successfully',
                ], $this-> successStatus
            ); 
        }
     }


     public function signupActivate($token)
        {
            $user = User::where('activation_token', $token)->first();

            if (!$user) {
                return response()->json([
                    'message' => 'This activation token is invalid.'
                ], 404);
            }

            $is_mentor = $user->is_mentor;
            $is_organization = $user->is_organization;
            $is_referrer = $user->is_referrer;

            $user->active = true;
            $user->activation_token = '';
            $user->save();
            $id = $user->id;

            if($is_mentor){
                Mail::to($user)->send(new WelcomeMail($user, 'mentor'));
            }elseif($is_organization){
                Mail::to($user)->send(new WelcomeMail($user, 'organization'));
            }elseif($is_referrer){
                Mail::to($user)->send(new WelcomeMail($user, 'referrer'));
            }else{
                Mail::to($user)->send(new WelcomeMail($user, 'mentee'));
            }
            
            $token = "Bearer"." ".$user->createToken('5minutes')-> accessToken;   
            return $this->get_user_details($id, $token);

        }

    public function resendActivation($email)
        {
            $user = User::where('email', $email)->first();

            if (!$user) {
                return response()->json([
                    'message' => 'error'
                ], 404);
            }

            $user->notify(new SignupActivate($user));
            
            return response()->json([
                'status' => 'successful',
                'data' => $user,
                'message' => 'Activation Link sent successfull',
            ], $this-> successStatus );
        }


/** 
     * get user details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function user(Request $request){
        $id = Auth::user()->id;
        $token = '';
        return $this->get_user_details($id, $token);
    }
    

     /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'status' => 'successful',
        ]);
    }
  
    
    function get_user_details($id, $token){
        $user = User::where('id',$id)->withCount(
            [
                'posts as allPostCount' => function ($query) {
                     $query->where('status', 1);
                    },
                'posts as postThisMonthCount' => function ($query) {
                    $query->where('status', 1)
                        ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()]);
                   },
                'followers as allFollowersCount' => function ($query) {
                    $query->where('is_accepted', 1);
                   },
                'followers as followersThisMonthCount' => function ($query) {
                    $query->where('is_accepted', 1);               
                        // ->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()]);
                   },
                'following as allFollowingCount' => function ($query) {
                    $query->where('is_accepted', 1);
                    },
                'following as followingThisMonthCount' => function ($query) {
                    $query->where('is_accepted', 1);           
                        // ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()]);
                    },
                'comment as allCommentCount',
                'comment as commentThisMonthCount',
                'views as allViewsCount',
                'views as viewsThisMonthCount',
                'likes as allLikesCount',
                'likes as likesThisMonthCount',
            ])->first(); 
        return response()->json(
            [
                'status' => 'successful',
                'data' => $user->load('status','interest','interest.category','social_links','organisation_profile','financial_profile'),
                'token' => $token
            ], 
            $this-> successStatus
        );
    }

}
