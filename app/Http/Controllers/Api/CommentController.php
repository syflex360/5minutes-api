<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Post;
use App\Traits\ApiJsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    public $successStatus = 201;
    use ApiJsonResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment = Comment::create([
            'user_id' => Auth::user()->id,
            'content' => $request->content,
            'post_id' => $request->post_id,
            'parent_id' => $request->parent_id,
        ]);

        $comment = Comment::where('post_id', $request->post_id)->with('user:id,name,avatar,slug','user.status')->get()->threaded();

        return response()->json(
            [
                'status' => 'successful',
                'data' => $comment,
                'message' => 'comment saved successfull',
            ], 
            $this-> successStatus
        ); 
        
        // try {
        //     $post = Post::where('id', '=', Input::get('post_id'))->first();
        //     $post->addComment([
        //         'content' => $request->get('content'),
        //         'parent_id' => $request->get('parent_id', null)
        //         ]);
        //     return response()->json($this->successResponse($post->getThreadedComments(),''));
        // } catch (\Exception $exception) {
        //     return response()->json($exception->getMessage());
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $comment = Comment::where('post_id', $id)->with('user:id,name,avatar,slug','user.status')->get()->threaded();
        return response()->json(
            [
                'status' => 'successful',
                'data' => $comment,
            ], 
            $this-> successStatus
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
