<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExclusiveSession;
use App\ExclusiveSessionPricing;
use App\ExclusiveSessionUser;
use App\Widthdrawable;
use App\Transaction;
use App\Invoice;
use App\User;
use Auth;


class ExclusiveSessionSubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get invoice and subscriptions cateory details
        $invoice = Invoice::where('id', $request->get('invoice_id'))->first();
       
         // insert into Subscription table
         $subscription = ExclusiveSessionUser::create([
             'user_id' => Auth::user()->id,
             'exclusive_session_id' => $invoice->subscribe_to,
         ]);

        //  get the 75 % due for mentor
        $amount_due = (75 / 100) * ($invoice->amount);

        //get mentor that ownes the exclusive session
        $session = ExclusiveSession::where('id', $invoice->subscribe_to)->first(); 
        $owner = User::where('id', $session->user_id)->first();

        // find owners widthdrawable or create new (get owners withdrwable details)
        $owner_widthdrawable = Widthdrawable::firstOrCreate(array('user_id' =>  $owner->id));

        // add amount due to mentors withdrawable if USD or Local Currency
        $widthdrawable_location = ($invoice->currency == 'USD') ? 'USD' : 'local' ;
        Widthdrawable::where('id', $owner_widthdrawable->id)
            ->update([
                $widthdrawable_location => $amount_due + $owner_widthdrawable->$widthdrawable_location,
            ]); 
        
 
         
         // insert transaction for the owners 75%
        $transaction = Transaction::create([
            'user_id' => Auth::user()->id,
            'mentor_id' => $owner->id,
            'subscription_id' => $session->id,
            'invoice_id' =>  $invoice->id,
            'transaction_type' => $invoice->type,
            'transaction' => 'credit',
            'currency_symbol' => $invoice->currency_symbol,
            'currency' => $invoice->currency,
            'amount_transacted' => $invoice->amount,
            'withdrawable_befor' => $owner_widthdrawable->$widthdrawable_location,
            'amount_due' => $amount_due,
            'withdrawable_after' => ($owner_widthdrawable->$widthdrawable_location) + ($amount_due),
            'description' =>  '',
        ]);
         
         return response()->json(
             [
                 'status' => 'successful',
                 'data' => $invoice,
                 'message' => 'Payment successfull',
             ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
