<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Invoice;
use App\User;
use DB;

class InvoiceController extends Controller
{
    public $successStatus = 201;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $data = Invoice::where('user_id', Auth::user()->id)->get();
        return response()->json(
            [
                'status' => 'successful',
                'data' => $data,
            ], 
            $this-> successStatus
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $input['user_id'] = (Auth::user()->id);
        $insert = Invoice::create($input);

        $invoice = Invoice::where('id', $insert->id)->first();
        
        return response()->json(
            [
                'status' => 'successful',
                'data' => $invoice,
            ], 
            $this-> successStatus
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {      
        // $update = Invoice::where("transRef",  $id)->first();        
        // $update->status = 1;
        // $update->save();

        // DB::table('subscriptions')->insert([
        //     'user_id' => (Auth::user()->id), 
        //     'mentor_id' => $update->mentor_id,
        //     'subscription_type_id' => $update->subscription_type_id, 
        //     'invoice_id' => $update->id,
        // ]);

        // $data = Invoice::where('id',$id)->with('user:id,name','subscription')->first();
        // return response()->json(
        //     [
        //         'status' => 'successful',
        //         'data' => $data,
        //     ], 
        //     $this-> successStatus
        // );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // if($request->get('type') === 'payment'){
        //     Invoice::where('transRef',$id)->update(['status' => 1]);
        //    $data = Invoice::where('transRef',$id)->first();
        //    return $data->transID;
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
