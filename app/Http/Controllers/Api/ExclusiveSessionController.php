<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExclusiveSession;
use App\ExclusiveSessionPricing;
use App\ExclusiveSessionUser;
use Auth;

class ExclusiveSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ExclusiveSession::with('exclusive_session_pricing','user:id,name,slug,avatar,is_mentor,is_organization')->get();
        return response()->json(
            [
                'status' => 'successful',
                'data' => $data,
                'message' => 'Exclusive fetched successfully'
            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // format date from (javascript date to php datetime)
         $initDateFrom = str_replace('T', ' ', $request->get('start_date'));
         $initDateFrom = substr($initDateFrom, 0, strpos($initDateFrom, '+'));
         $dateFrom = date('Y-m-d h:i:s', strtotime($initDateFrom));

         // format date to (javascript date to php datetime)
         $initDateTo = str_replace('T', ' ', $request->get('end_date'));
         $initDateTo = substr($initDateTo, 0, strpos($initDateTo, '+'));
         $dateTo = date('Y-m-d h:i:s', strtotime($initDateTo));
 
         $ex = ExclusiveSession::create([
             'user_id' => Auth::user()->id,
             'name' => $request->get('name'),
             'avatar' => $request->get('avatar'),
             'description' => $request->get('description'),
             'number_of_users' => $request->get('number_of_users'),
             'start_date' => $dateFrom,
             'end_date' => $dateTo,
         ]);
         

         $exp_data = [
            [
                'exclusive_session_id' =>  $ex->id,
                'currency_symbol' => '$',
                'currency' => 'USD',
                'amount' => $request->get('USD'),
                'charges' => (0.28) + ((2.5 / 100) * $request->get('USD'))
            ],
            [
                'exclusive_session_id' =>  $ex->id,
                'currency_symbol' => '₦',
                'currency' => 'NGN',
                'amount' => $request->get('local'),
                'charges' => (100) + ((1.5 / 100) * $request->get('local'))
             ]
         ];
         

         $exp = ExclusiveSessionPricing::insert($exp_data);
 
         return response()->json(
         [
             'status' => 'successful',
             'data' => $ex->load('exclusive_session_pricing','user:id,name,slug,avatar,is_mentor,is_organization'),
             'message' => 'Exclusive Session saved and fetched successfully'
         ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id == 'myES') {
            $data = ExclusiveSession::where('user_id', Auth::user()->id)->with('exclusive_session_pricing','user:id,name,slug,avatar,is_mentor,is_organization')->get();
        }elseif ($id == 'mySES') {
            $data = ExclusiveSessionUser::where('user_id', Auth::user()->id)->with('exclusive_session','exclusive_session.user:id,name,slug,avatar,is_mentor,is_organization')->get();
        }else{
            $data = [];
        }
        
        return response()->json(
            [
                'status' => 'successful',
                'data' => $data,
                'message' => 'Exclusive fetched successfully'
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
