<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Avatar;
use Storage;
use App\Notifications\SignupActivate;
use Carbon\Carbon;
use App\User; 
use App\Post;
use App\Invoice;
use App\Mail\WelcomeMail;
use App\Mail\PaymentReceipt;
use App\Mail\MentorsPreconfirmationMessage;
use App\Notifications\WelcomeNotification;
use Illuminate\Support\Facades\Mail;
use Auth;
use Spatie\Searchable\Search;

class testController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        // sending email with custom email test successfully
        
        // $user = User::where('id', 1)->first();
        // $invoice = Invoice::where('id', 82)->first();
        // Mail::to($user)->send(new PaymentReceipt($user, $invoice));
        
        // Mail::to($user)->send(new WelcomeMail($user, 'mentee'));
        // Mail::to($user)->send(new MentorsPreconfirmationMessage($user));
        // $user->notify(new SignupActivate($user));

        // $user->notify(new WelcomeNotification($user));

        // amazon s3 test completed
        // $avatar = Avatar::create('mr simon')->getImageObject()->encode('png');
        // $disk = Storage::disk('s3');
        // $user = Storage::disk('s3')->put('profile/6/avatar.png', (string) $avatar, 'public');
        return env('RAVE_PUBLIC_KEY', null);//$user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $searchterm = $request->search;
 
        $searchResults = (new Search())
                    ->registerModel(User::class, 'name')
                    ->registerModel(Post::class, 'title')
                    ->perform($searchterm);
 
        return $searchResults;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $searchterm = $id;
 
        $searchResults = (new Search())
                    ->registerModel(User::class, 'title')
                    // ->registerModel(Event::class, 'title')
                    ->perform($searchterm);
 
        return $searchResults;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
