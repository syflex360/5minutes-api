<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\SocialAccountService;

class SocialLoginController extends Controller
{
    public function redirectToSocial($social)
    {
        return Socialite::driver($social)->redirect();
    }

    function handleSocialCallback()
    {        
        return Socialite::driver('facebook')->stateless()->user();
        // try {
        //     $user = $service->setOrGetUser(Socialite::driver($social));
        //     auth()->login($user);
        //     return redirect('/feed');
        // } catch (\Exception $e) {
        //     \Log::error('social login error: '.$e);
        //     return $e;
        // }
    }
        
}
