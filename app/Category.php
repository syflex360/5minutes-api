<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','description', 'avatar',];
    
    public function posts(){
        return $this->hasMany(Post::class);
    }

    public function interest(){
        return $this->hasOne(Interest::class);
    }
    
}
