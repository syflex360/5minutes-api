<?php

namespace App;

use Elasticquent\ElasticquentTrait;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Forum;
use Storage;

class User extends Authenticatable implements Searchable
{
    use Notifiable, HasApiTokens, SoftDeletes, Sluggable, ElasticquentTrait;

    protected $dates = ['deleted_at'];

    public function getSearchResult(): SearchResult
    {
    //    $url = route('api/test', $this->id);
    
        return new \Spatie\Searchable\SearchResult(
           $this,
           $this->name
        //    $url
        );
    }   
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'email', 'password', 'about', 'birthday', 'avatar', 'active', 'activation_token', 
        'is_mentor', 'is_organization','referrer_code','is_referrer', 'country_id','currency_symbol','currency'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];


    public function profile()
    {
        return $this->hasOne('App\SocialProfile');
    }
    
    public function posts(){
        return $this->hasMany(Post::class);
    }

    
    public function interest(){
        return $this->hasMany(Interest::class);
    }

    public function social_links(){
        return $this->hasMany(SocialLink::class);
    }

    public function organisation_profile(){
        return $this->hasMany(Organisation::class);
    }

    public function financial_profile(){
        return $this->hasMany(FinancialProfile::class);
    }

    public function status(){
        return $this->hasMany(Achievement::class)->where('is_default', 1);
    }

    public function achievement(){
        return $this->hasMany(Achievement::class);
    }

    public function DirectMessage(){
        return $this->hasMany(DirectMessage::class);
    }

    public function UnreadDirectMessage(){
        return $this->hasMany(DirectMessage::class)->where('status', 0);
    }

    public function forumConversation(){
        return $this->hasMany(ForumConversation::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'mentor_id', 'user_id')
                    ->withPivot('mentor_id', 'user_id')
                    ->withTimestamps();
    }

    public function following()
    {
        return $this->belongsToMany(User::class, 'followers', 'user_id', 'mentor_id')
                    ->withPivot('mentor_id', 'user_id')
                    ->withTimestamps();
    }

    public function isFollowing($user_id = null)
    {
        return $this->following()
            ->where('mentor_id', $user_id ?: auth()->id() )
            ->exists();
    }

    public function getIsFollowedAttribute()
    {
        return $this->followers()
            ->where('mentor_id', $this->getKey() )
            ->exists();
    }


    public function subscription(){
        return $this->hasMany(Subscription::class);
    }
    

    function role(){
        return $this->belongsTo('App\Role');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function forums()
    {
        return $this->belongsToMany(Forum::class)->withTimestamps();
    }
    
    public function invoice()
    {
        return $this->belongsToMany(Invoice::class);
    }

    public function comment()
    {
        return $this->hasManyThrough('App\Comment', 'App\Post');
    }

    public function views()
    {
        return $this->hasManyThrough(
            'App\View',
            'App\Post',
            'user_id', // Foreign key on post table...
            'Viewable_id', // Foreign key on Views table...
            'id', // Local key on users table...
            'id' // Local key on Posts table...
        );
    }

    public function likes()
    {
        return $this->hasManyThrough(
            'App\Like',
            'App\Post',
            'user_id', // Foreign key on post table...
            'likeable_id', // Foreign key on Like table...
            'id', // Local key on users table...
            'id' // Local key on Posts table...
        );
    }

    //elasticserach testing
    //index configuration
    /**
     * The elasticsearch settings.
     *
     * @var array
     */
    protected $indexSettings = [
        'analysis' => [
            'char_filter' => [
                'replace' => [
                    'type' => 'mapping',
                    'mappings' => [
                        '&=> and '
                    ],
                ],
            ],
            'filter' => [
                'word_delimiter' => [
                    'type' => 'word_delimiter',
                    'split_on_numerics' => false,
                    'split_on_case_change' => true,
                    'generate_word_parts' => true,
                    'generate_number_parts' => true,
                    'catenate_all' => true,
                    'preserve_original' => true,
                    'catenate_numbers' => true,
                ]
            ],
            'analyzer' => [
                'default' => [
                    'type' => 'custom',
                    'char_filter' => [
                        'html_strip',
                        'replace',
                    ],
                    'tokenizer' => 'whitespace',
                    'filter' => [
                        'lowercase',
                        'word_delimiter',
                    ],
                ],
            ],
        ],
    ];

    protected $mappingProperties = array(
        'name' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'email' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'password' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
    );

    function getIndexName()
    {
        return $this->getTable();
    }

}
