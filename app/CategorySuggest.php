<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategorySuggest extends Model
{
    protected $fillable = ['name','description', 'user_id',];
}
