<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    Protected $fillable = ['user_id','likeable_id','likeable_type'];
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function post()
    {
        return $this->belongsTo(Post::class, 'likeable_id');
    }

    
}