<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Forum;
use App\User;

class ForumConversation extends Model
{
    protected $fillable = ['message', 'user_id', 'forum_id'];

    protected $appends = ['ReadableDate'];
    public function getReadableDateAttribute(){
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']))->diffForHumans();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }
}
