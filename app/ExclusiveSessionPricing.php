<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExclusiveSessionPricing extends Model
{
    protected $fillable = [
        'exclusive_session_id','currency','currency_symbol','amount'
    ];

    public function exclusive_session()
    {
        return $this->belongsTo(ExclusiveSession::class, 'exclusive_session_id');
    }
    
}
