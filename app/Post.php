<?php

namespace App;

use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Carbon\Carbon;
use App\Like;

class Post extends Model implements Searchable
{
    use Sluggable;
    use ElasticquentTrait;
    
    protected $fillable = [
        'user_id','title', 'content', 'content_type', 'status','published_at','category_id','allow_comment'
        ,'is_premium','is_signed','is_exclusive','exclusive_session_id'
    ];

    protected $appends = ['ReadableDate'];
    public function getReadableDateAttribute(){
        return Carbon::createFromTimeStamp(strtotime($this->attributes['published_at']))->diffForHumans();
    }

    public function getSearchResult(): SearchResult
    {
    //    $url = route('api/search_post', $this->id);
    
        return new \Spatie\Searchable\SearchResult(
           $this,
           $this->title
        //    $url
        );
    }   

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function postMedia()
        {
            return $this->hasMany(PostMedia::class);
        }

        public function category()
        {
            return $this->belongsTo(Category::class, 'category_id');
        }
    
        public function comments()
        {
            return $this->hasMany(Comment::class);
        }

        public function like()
        {
            return $this->hasMany(Like::class, 'likeable_id');
        }

        public function afterThought()
        {
            return $this->hasMany(AfterThought::class);
        }
    
        public function getThreadedComments(){
            return $this->comments()->with('user:id,name,avatar,slug','user.status')->get()->threaded();
        }
    
        public function addComment($attributes)
        {
            $comment = (new Comment())->forceFill($attributes);
            $comment->user_id = auth()->id();
            return $this->comments()->save($comment);
        }

        /**
         * Return the sluggable configuration array for this model.
         *
         * @return array
         */
        public function sluggable()
        {
            return [
                'slug' => [
                    'source' => 'title'
                ]
            ];
        }

    //elasticserach testing
    //index configuration
    /**
     * The elasticsearch settings.
     *
     * @var array
     */
    protected $indexSettings = [
        'analysis' => [
            'char_filter' => [
                'replace' => [
                    'type' => 'mapping',
                    'mappings' => [
                        '&=> and '
                    ],
                ],
            ],
            'filter' => [
                'word_delimiter' => [
                    'type' => 'word_delimiter',
                    'split_on_numerics' => false,
                    'split_on_case_change' => true,
                    'generate_word_parts' => true,
                    'generate_number_parts' => true,
                    'catenate_all' => true,
                    'preserve_original' => true,
                    'catenate_numbers' => true,
                ]
            ],
            'analyzer' => [
                'default' => [
                    'type' => 'custom',
                    'char_filter' => [
                        'html_strip',
                        'replace',
                    ],
                    'tokenizer' => 'whitespace',
                    'filter' => [
                        'lowercase',
                        'word_delimiter',
                    ],
                ],
            ],
        ],
    ];

    protected $mappingProperties = array(
        'title' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'content' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'content_type' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
    );

    function getIndexName()
    {
        return $this->getTable();
    }
}
