<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionType extends Model
{
    protected $fillable = [
        'name','type','price','charges','currency','currency_symbol','planId','days','description'
    ];

    public function invoice()
    {
        return $this->belongsToMany(Invoice::class);
    }

    public function subscription(){
        return $this->hasMany(Subscription::class);
    }
}
