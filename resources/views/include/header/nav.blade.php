
                <nav class="navbar navbar-expand navbar-dark bg-info fixed-top">
                        <div class="container d-flex justify-content-between">
                        
                        <div class="d-flex">
                                <a class="navbar-brand" href="{{url('./')}}">5minutes</a>
                                <a class="navbar-brand" href="{{url('./')}}">Home</a>
                        </div>
        
                        <div class="d-flex d-none d-lg-block">
                            @auth
                            <form class="form-inline my-2 my-md-0">
                                <input class="form-control" type="text" placeholder="Search">
                            </form>
                            @endauth
                        </div>
        
                        <div class="d-flex d-none d-lg-block">
                            @auth
                                <ul class="navbar-nav">
                                        <li class="nav-item dropdown active">
                                                <a class="nav-link dropdown-toggle" href="#" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Message {{auth()->user()->unreadNotifications->count()}}</a>
                                                <div class="dropdown-menu" aria-labelledby="dropdown03">
                                                  <div class="drop-title">Notifications</div>

                                                  @foreach(auth()->user()->unreadNotifications as $nitification)                                                
                                                  <!-- Message -->
                                                  <a href="#">
                                                      <div class="user-img btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                      <div class="mail-contnet">
                                                          <h5>{{Auth()->user()->name}}</h5> <span class="mail-desc"> {{$nitification->data['body']}}!</span> <span class="time"> {{$nitification['created_at']}}</span> 
                                                      </div>
                                                  </a>                                           
                                                  @endforeach  
      
                                                  @foreach(auth()->user()->readNotifications as $nitification)                                                
                                                  <!-- Message -->
                                                  <a href="#">
                                                      <div class="user-img btn btn-danger btn-circle text-muted"><i class="fa fa-link"></i></div>
                                                      <div class="mail-contnet text-muted">
                                                          <h5>{{Auth()->user()->name}}</h5> <span class="mail-desc"> {{$nitification->data['body']}}!</span> <span class="time"> {{$nitification['created_at']}}</span> 
                                                      </div>
                                                  </a>                                           
                                                  @endforeach   

                                                </div>
                                         </li>
                                        <li class="nav-item dropdown active">
                                                <a class="nav-link dropdown-toggle" href="#" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Notifications</a>
                                                <div class="dropdown-menu" aria-labelledby="dropdown03">
                                                 <message-notifications></message-notifications>
                                                </div>
                                         </li>                               
                                        <li class="nav-item dropdown active">
                                                <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <strong class="hidden-sm-down">{{Auth::user()->name}} </strong><img src="{{url('/storage/'.Auth::user()->avatar)}}" alt="{{Auth::user()->name}}" class="profile-pic" width="30"/>
                                                        </a>                                       
                                                <div class="dropdown-menu" aria-labelledby="dropdown03">
                                                        <ul class="dropdown-user">
                                                                <li>
                                                                    <div class="dw-user-box">                                                                       
                                                                        <div class="u-text">
                                                                            <h4> {{ Auth::user()->name }}</h4>
                                                                            <p class="text-muted"> {{ Auth::user()->email }}</p>
                                                                         </div>
                                                                    </div>
                                                                </li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="{{url('/5', Auth::user()->slug)}}"><i class="ti-user"></i> My Profile</a></li>
                                                                <li><a href="{{url('/forum')}}"><i class="ti-wallet"></i> Forums</a></li>
                                                                <li><a href="{{url('/message')}}"><i class="ti-email"></i> Message</a></li>
                                                                <li><a href="{{url('/chat')}}"><i class="ti-email"></i> Chat</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="{{url('/subscription')}}"><i class="ti-email"></i> subscription</a></li>
                                                                <li><a href="{{url('/invoice')}}"><i class="ti-email"></i> invoice</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="{{url('/5', Auth::user()->slug) }}"><i class="ti-settings"></i> Account Setting</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="{{ route('logout') }}"
                                                                   onclick="event.preventDefault();
                                                                                 document.getElementById('logout-form').submit();">
                                                                                 <i class="fa fa-power-off"></i> {{ __('Logout') }}</a>
                                                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                    @csrf
                                                                </form>
                                                                </li>
                                                            </ul>
                                                </div>
                                         </li>
                                </ul>
                                @else
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item active">
                                          <a class="nav-link" href="{{url('login')}}">Login </span></a>
                                        </li>
                                        <li class="nav-item active">
                                          <a class="nav-link" href="{{url('register')}}">Register</a>
                                        </li>                                                                              
                                    </ul>
                                @endauth
                        </div>
                              
                        </div>
                    </nav>    
    