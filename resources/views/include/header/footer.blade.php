<div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-unfold mr-2"></i>
                Footer
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                &copy; 2018. <a href="#">5minutes</a>
            </span>

            <ul class="navbar-nav ml-lg-auto">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">The Mentor, Mentee platform</a>
                </li>

                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-lifebuoy"></i>
                    </a>
                </li>

               
            </ul>
        </div>
    </div>