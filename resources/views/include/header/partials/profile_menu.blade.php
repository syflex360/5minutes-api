<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
                        <span>{{Auth::user()->name}}</span>
                        <img src="{{url('/storage/'.Auth::user()->avatar)}}" class="rounded-circle" alt="{{Auth::user()->name}}">						
					</a>

					<div class="dropdown-menu dropdown-menu-right">
					<a href="{{url('/5', Auth::user()->slug) }}" class="dropdown-item"><i class="icon-user"></i> Profile</a>
                        <a href="{{url('/forum')}}" class="dropdown-item"><i class="icon-user-plus"></i> Forums</a>
                        <a href="{{url('/message')}}" class="dropdown-item"><i class="icon-user-plus"></i> Message</a>
                        <a href="{{url('/chat')}}" class="dropdown-item"><i class="icon-user-plus"></i> Chat</a>
                         <div class="dropdown-divider"></div>
                        <a href="{{url('/subscription')}}" class="dropdown-item"><i class="icon-user"></i> subscription</a>
                        <a href="{{url('/invoice')}}" class="dropdown-item"><i class="icon-user"></i> invoice</a>
                         <div class="dropdown-divider"></div>                               
                        <a href="{{url('/5', Auth::user()->slug) }}" class="dropdown-item"><i class="icon-switch"></i> Account Setting</a>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();" class="dropdown-item">
                                     <i class="icon-switch2"></i>
                                      {{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
					</div>
				</li>