<li class="nav-item dropdown">
                   <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                       <i class="icon-bell2"></i> Notification
                       <span class="badge badge-pill bg-danger-400 position-static ml-auto ml-xl-2">{{auth()->user()->unreadNotifications->count()}}</span>
                   </a>
                   
                   <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                       <div class="dropdown-content-header">
                           <span class="font-weight-semibold">Latest activity {{auth()->user()->unreadNotifications->count()}}</span>
                           <!-- <a href="#" class="text-default"><i class="icon-search4 font-size-base"></i></a> -->
                       </div>

                       <div class="dropdown-content-body dropdown-scrollable">
                           <ul class="media-list">
                                          
                            @foreach(auth()->user()->unreadNotifications as $notification)         
                               <li class="media">
                                   <div class="mr-3">
                                       <a href="#"><img src="{{url('/storage/'.$notification->data['avatar'])}}" alt="{{$notification->data['name']}}" class="rounded-circle" width="35" height="35"></a>
                                   </div>

                                   <div class="media-body">
                                       <a href="#">{{$notification->data['name']}}</a> {{$notification->data['body']}}
                                       <div class="font-size-sm text-muted mt-1">4 minutes ago</div>
                                   </div>
                               </li>
                            @endforeach  
                           </ul>
                       </div>

                       <div class="dropdown-content-footer bg-light">
                           @if(auth()->user()->unreadNotifications)
                           <a href="#" class="text-grey mr-auto">All activity</a>
                           @endif
                           <div>
                                @if(auth()->user()->unreadNotifications)
                               <a href="#" class="text-grey" data-popup="tooltip" title="Clear list"><i class="icon-checkmark3"></i></a>
                               @endif
                               <a href="#" class="text-grey ml-2" data-popup="tooltip" title="Settings"><i class="icon-gear"></i></a>
                           </div>
                       </div>
                   </div>
               </li>