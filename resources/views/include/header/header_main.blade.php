 <!-- Multiple fixed navbars wrapper -->
 <div class="fixed-top">

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-info">

    <div class="navbar-brand wmin-200">
        <a href="{{('/')}}" class="d-inline-block">
           <img src="{{asset('assets/images/logo_light.png')}}" alt="">
        </a>
    </div>

    <div class="d-md-none">               
        <button class="navbar-toggler" type="button" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>                
        <button class="navbar-toggler" type="button" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler">
                <a href="#" class="" data-toggle="dropdown">
                    <img src="{{asset('assets/images/placeholders/placeholder.jpg')}}" class="rounded-circle" alt="" width="20px">                                
                </a>        
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                    <a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
                    <a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
                    <a href="#" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                </div>
        </button>    
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="{{url('/')}}" class="navbar-nav-link active">
                    <i class="icon-home"></i><strong> Home</strong>
                </a>
            </li>

            <nav-search><nav-search>
            
        </ul>

        <span class="navbar-text ml-md-auto mr-md-2">
           
        </span>

        @auth
        <ul class="navbar-nav">

            <message-notifications></message-notifications>

            @include('include.header.partials.notifications')
               

               <div class="nav-item dropdown dropdown-user">
                    

                   <a href="{{url('/5', Auth::user()->slug)}}" class="navbar-nav-link" data-toggle="dropdown">
                       <img src="{{url('/storage/'.Auth::user()->avatar)}}" alt="{{Auth::user()->name}}" class="rounded-circle">
                       <span>{{Auth::user()->name}}</span>
                   </a>

                   <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{url('/5', Auth::user()->slug) }}" class="dropdown-item"><i class="icon-user"></i> Profile</a>
                        <a href="{{url('/forum')}}" class="dropdown-item"><i class="icon-user-plus"></i> Forums</a>
                        <a href="{{url('/message')}}" class="dropdown-item"><i class="icon-user-plus"></i> Message</a>
                        <a href="{{url('/chat')}}" class="dropdown-item"><i class="icon-user-plus"></i> Chat</a>
                         <div class="dropdown-divider"></div>
                        <a href="{{url('/subscription')}}" class="dropdown-item"><i class="icon-user"></i> subscription</a>
                        <a href="{{url('/invoice')}}" class="dropdown-item"><i class="icon-user"></i> invoice</a>
                         <div class="dropdown-divider"></div>                               
                        <a href="{{url('/5', Auth::user()->slug) }}" class="dropdown-item"><i class="icon-switch"></i> Account Setting</a>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();" class="dropdown-item">
                                     <i class="icon-switch2"></i>
                                      {{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                   </div>
               </div>
           </ul>
        @else
            <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="navbar-nav-link" href="{{url('login')}}">Login </span></a>
                </li>
                <li class="nav-item">
                  <a class="navbar-nav-link" href="{{url('register')}}">Register</a>
                </li>                                                                              
            </ul>
        @endauth            
</div>
</div>
<!-- /main navbar -->


<!-- Alternative navbar -->        
{{-- <div class="navbar navbar-expand-md navbar-light">
    <div class="text-center d-md-none w-100">
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-second">
            Message
        </button>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-second">
                Bookmark
            </button>
            <button type="button" class="navbar-toggler " data-toggle="collapse" data-target="#navbar-second">
                    Notifications
                </button>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-second">
                        Pined
                    </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-second">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-git-branch mr-2"></i>
                    Branches
                </a>
            </li>					
        </ul>

        <ul class="navbar-nav ml-md-auto">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-repo-forked mr-2"></i>
                    Repositories
                    <span class="badge badge-pill bg-teal-800 position-static ml-auto ml-md-2">28</span>
                </a>
            </li>
        </ul>
    </div>
</div> --}}
<!-- /alternative navbar -->

</div>
<!-- /multiple fixed navbars wrapper -->
<br>
<br>




