
<div class="navbar navbar-expand-md navbar-dark bg-info navbar-static fixed-top pr-5 pl-5">
		
		<div class="navbar-brand">
			<a href="{{url('./')}}" class="d-inline-block">
				<img src="{{asset('assets/images/logo_light.png')}}" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile" aria-expanded="true">
				<i class="icon-paragraph-justify3"></i>
			</button>			
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile" style="">
			<ul class="navbar-nav">
				<li class="nav-item">
					@auth
					<a href="{{url('feed')}}" class="navbar-nav-link legitRipple">
						<i class="icon-home"></i> Home
					</a>
					@else
					<a href="{{url('./')}}" class="navbar-nav-link legitRipple">
						<i class="icon-home"></i> Home
					</a>
					@endauth
				</li>
			</ul>
		@auth
			<div class="">
				<i class="icon-search"></i>
				<input class="" type="text" placeholder="search">
			</div>
			
		@endauth
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link legitRipple">
						Support
					</a>
				</li>
				@auth
					<message-notifications></message-notifications>
					@include('include.header.partials.notifications')					
					@include('include.header.partials.profile_menu')
				@else
					<li class="nav-item">
						<a href="{{url('login')}}" class="navbar-nav-link legitRipple">
							Login
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('register')}}" class="navbar-nav-link legitRipple">
							Register
						</a>
					</li>
				@endauth
				
			</ul>
		</div>
	</div>