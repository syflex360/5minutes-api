@extends('layouts.app')

@section('content')

	<!-- Content area -->
  <div class="content d-flex justify-content-center align-items-center">

<!-- Login form -->
<form class="login-form" method="POST" action="{{ route('login') }}">
@csrf
  <div class="card mb-0">
    <div class="card-body">
      <div class="text-center mb-3">        
        <h5 class="mb-0">Login to your account</h5>
        <span class="d-block text-muted">Your credentials</span>
      </div>

      <div class="form-group form-group-feedback form-group-feedback-left">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email Address" value="{{ old('email') }}" required autofocus>
        <div class="form-control-feedback">
          <i class="icon-user text-muted"></i>
        </div>
        @if ($errors->has('email'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
         @endif
      </div>

      <div class="form-group form-group-feedback form-group-feedback-left">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
        <div class="form-control-feedback">
          <i class="icon-lock2 text-muted"></i>
        </div>
        @if ($errors->has('password'))
          <span class="invalid-feedback">
          <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>

      <div class="form-group d-flex align-items-center">
        <div class="form-check mb-0">
          <label class="form-check-label">
            <input type="checkbox" name="remember" class="form-input-styled" {{ old('remember') ? 'checked' : '' }} checked>
            Remember
          </label>
        </div>

        <a href="{{ route('password.request') }}" class="ml-auto">Forgot password?</a>
      </div>


      <div class="form-group">
        <button type="submit" class="btn btn-info btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
      </div>

      <div class="form-group text-center text-muted content-divider">
        <span class="px-2">or sign in with</span>
      </div>

      <div class="form-group text-center">
        <a href="{{ url('/auth/social/facebook') }}"class="btn btn-outline bg-indigo border-indigo text-indigo btn-icon rounded-round border-2"><i class="icon-facebook"></i></button>
        <a href="{{ url('/auth/social/google') }}" class="btn btn-outline bg-danger-600 border-danger-600 text-danger-600 btn-icon rounded-round border-2 ml-2"><i class="icon-google"></i></a>
      </div>

      <div class="form-group text-center text-muted content-divider">
        <span class="px-2">Don't have an account?</span>
      </div>

      <div class="form-group">
        <a href="{{ url('register') }}" class="btn btn-light btn-block">Sign up</a>
      </div>

      <span class="form-text text-center text-muted">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>
    </div>
  </div>
</form>
<!-- /login form -->

</div>
<!-- /content area -->

   @endsection