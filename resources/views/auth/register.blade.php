@extends('layouts.app')

@section('content')

<!-- Content area -->
<div class="content d-flex justify-content-center align-items-center">

<!-- Registration form -->
<form class="flex-fill" method="POST" action="{{ route('register') }}">
            @csrf
  <div class="row">
    <div class="col-lg-6 offset-lg-3">
      <div class="card mb-0">
        <div class="card-body">
          <div class="text-center mb-3">
            <i class="icon-plus3 icon-2x text-info border-info border-3 rounded-round p-3 mb-3 mt-1"></i>
            <h5 class="mb-0">Create account</h5>
            <span class="d-block text-muted">All fields are required</span>
          </div>

          <div class="form-group form-group-feedback form-group-feedback-right">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Full Name" value="{{ old('name') }}" required autofocus>
            <div class="form-control-feedback">
              <i class="icon-user-plus text-muted"></i>
            </div>
            @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-group-feedback form-group-feedback-right">
                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="Mobile Number" value="{{ old('phone') }}" required>
                <div class="form-control-feedback">
                  <i class="icon-mention text-muted"></i>
                </div>
                @if ($errors->has('phone'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group form-group-feedback form-group-feedback-right">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email Address" value="{{ old('email') }}" required>
                <div class="form-control-feedback">
                  <i class="icon-mention text-muted"></i>
                </div>
                @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-md-6">
              <div class="form-group form-group-feedback form-group-feedback-right">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                <div class="form-control-feedback">
                  <i class="icon-user-lock text-muted"></i>
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group form-group-feedback form-group-feedback-right">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                <div class="form-control-feedback">
                  <i class="icon-user-lock text-muted"></i>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">            
            <div class="form-check">
              <label class="form-check-label">
                <input type="checkbox" class="form-input-styled" checked>
                Subscribe to monthly newsletter
              </label>
            </div>

            <div class="form-check">
              <label class="form-check-label">
                <input type="checkbox" class="form-input-styled" >
                Accept <a href="#">terms of service</a>
              </label>
            </div>
          </div>

          <button type="submit" class="btn bg-info-400 btn-labeled btn-labeled-right"><b><i class="icon-plus3"></i></b> Create account</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- /registration form -->

</div>
<!-- /content area -->



@endsection
