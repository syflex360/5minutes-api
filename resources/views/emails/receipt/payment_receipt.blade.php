<head>
<!-- Styles -->
<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .row {
                width: 100%;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 32px;
            }

            .links > a {
                color: #FFF;
                padding: 0 8px;
                font-size: 12px;
                /* font-weight: 600; */
                /* letter-spacing: .1rem; */
                text-decoration: none;
                /* text-transform: uppercase; */
            }

            .text-white {
                color: #FFF;
            }

            .back-pad {
                background-color: #006400; padding: 10px 10px 10px 10px;
            }
        </style>
    </head>
<body>  

<div class="content row">
    <h2>{{$user['name']}}</h2>
</div>

<div class="flex-center back-pad"></div>

<div class="title content row">
    <h4>
        Payment Receipt from <a href="https://www.5minutes.one/">5minutes</a>        
    </h4>
</div> 

<p style="font-size: 14px"><b>Your payment was successful and has been recieved by <a href="https://www.5minutes.one/">5minutes</a></b></p>

<div class="content row">
    <h2>{{$invoice['currency']}}  {{$invoice['amount']}}</h2>
</div>

<table>
    <tbody>
        <tr>
            <td>Amount Paid: </td>
            <td><h2>{{$invoice['amount']}}</h2></td>
        </tr>
        <tr>
            <td>Transaction Ref: </td>
            <td><h5>{{$invoice['transID']}}</h5></td>
        </tr>
    </tbody>
</table>

<div class="content row">
    <h2>{{$invoice['created_at']->format('d M, Y')}}</h2>
</div>

<p>
if you have questions or issues with this payment, contact <a href="mailto:support@5minutes.one">support@5minutes.one</a>
</p>

<div class="flex-center back-pad">
    <div class="content links row">
        <a href="https://www.5minutes.one/auth/login">Login</a>|
        <a href="https://www.5minutes.one/about">About Us</a>|
        <a href="https://www.5minutes.one/contact">Contact Us</a> 
    </div>    
</div>

<p class="content" style="font-size: 8px;">Copyright (c) 2019 5minutes.</p>

</body>
