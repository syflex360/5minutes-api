<head>
<!-- Styles -->
<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .row {
                width: 100%;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 32px;
            }

            .links > a {
                color: #FFF;
                padding: 0 8px;
                font-size: 12px;
                /* font-weight: 600; */
                /* letter-spacing: .1rem; */
                text-decoration: none;
                /* text-transform: uppercase; */
            }

            .text-white {
                color: #FFF;
            }

            .back-pad {
                background-color: #006400; padding: 10px 10px 10px 10px;
            }
        </style>
    </head>
<body>  
<div class="flex-center back-pad">
    <div class="title content row">
        <a href="https://www.5minutes.one/" class="text-white content"><b>5minutes</b></a>
    </div> 
</div>

<p style="font-size: 18px"><b>Hi {{$user['name']}}</b></p>
<p style="font-size: 20px"><b>Welcome to 5minutes, the mentor-mentee platform.</b></p>
<p>
    We are excited to have you join this global Straight-to-the-Point 
    learning and mentoring environment for continuous and consistent life 
    and career upgrade, following your own preferred mentor.
</p>
<p>
    There are two ways to learn anything: Go to school and learn theories OR 
    find a mentor who’s been there and done that, and avoid all the potholes.
</p>
<p>
 <b>
    Here are a few Points to jumpstart your 5minutes experience:
 </b>
</p>

<ul>
 <li>Find your preferred mentors, follow them and subscribe to enjoy their PREMIUM Contents</li>
 <li>Mentors share FREE and PREMIUM Contents</li>
 <li>Subscription fee for any mentor is $3/month OR N1000/month. Annual subscription discounts 1month) – Pay Attention!</li>
 <li>You can ONLY view FREE Contents for any mentor you’re following if you have not subscribed to them</li>
 <li>PREMIUM Contents are the Real Deal and are usually in series</li>
 <li>Text articles are no more than 230 words</li>
 <li>Video Contents are no more than 2minutes, 30seconds </li>
 <li>After Thoughts are posted after each Main Article</li>
 <li>You can partake in Exclusive Sessions organized by mentors (rates differ)</li>
 <li>You can have Forum Discussions, Chats and Messages</li>
 <li>You can Like posts, Bookmark or Report Abusive Posts</li>
 <li>You can Become a Referrer too and refer mentors which earns you 5% of their monthly accumulated subscriptions for the next 12months</li>
 <li>There’s so much more you can do on 5minutes</li> 
</ul>

<p>Okay, let’s go get your profile set up and start your transformation process to Becoming Better.</p>
<p>I’m your man,</p><br>
<p>Isah Raphael <br>
(Founder, 5minutes)
</p>

<div class="flex-center back-pad">
    <div class="content links row">
        <a href="https://www.5minutes.one/auth/login">Login</a>|
        <a href="https://www.5minutes.one/about">About Us</a>|
        <a href="https://www.5minutes.one/contact">Contact Us</a> 
    </div>    
</div>

<p class="content" style="font-size: 8px;">Copyright (c) 2019 5minutes.</p>

</body>
