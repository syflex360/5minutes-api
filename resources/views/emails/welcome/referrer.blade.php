<head>
<!-- Styles -->
<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .row {
                width: 100%;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 32px;
            }

            .links > a {
                color: #FFF;
                padding: 0 8px;
                font-size: 12px;
                /* font-weight: 600; */
                /* letter-spacing: .1rem; */
                text-decoration: none;
                /* text-transform: uppercase; */
            }

            .text-white {
                color: #FFF;
            }

            .back-pad {
                background-color: #006400; padding: 10px 10px 10px 10px;
            }
        </style>
    </head>
<body>  
<div class="flex-center back-pad">
    <div class="title content row">
        <a href="https://www.5minutes.one/" class="text-white content"><b>5minutes</b></a>
    </div> 
</div>

<p style="font-size: 16px"><b>Hi {{$user['name']}}</b></p>
<p><b style="font-size: 18px">Welcome to 5minutes, the mentor-mentee platform. You’re part of the family now.</b></p>
<p>
    This is your Referrer Code: <strong>{{$user['referrer_code']}}</strong>
</p>
<p>
    Being a Referrer is one of the coolest things that can happen on 5minutes. 
    Not only are you contributing to the improvement of learning globally by referring 
    mentors to teach on the 5minutes platform, you also get to earn Referrer Incentives 
    from their proceeds for the next 12months. Isn’t that just beautiful?
</p>
<p>Referrer Incentive = 5% of referred mentors monthly accumulated subscription for the next 12months.</p>
<p>
 <b>
    Here’s what you have to do:
 </b>
</p>

<ul>
 <li>-	Refer Mentors Using your Referrer Code: {{$user['referrer_code']}} (So we know it’s you).</li>
 <li>-	You can view mentors performance from your Referrer Dashboard</li>
 <li>-	Set up your Financial Account Details for collection of Incentives</li>
</ul>

<p>Okay, let’s get to work!</p>
<p>Your man,</p><br>
<p>Isah Raphael <br>
(Founder, 5minutes)
</p>

<div class="flex-center back-pad">
    <div class="content links row">
        <a href="https://www.5minutes.one/auth/login">Login</a>|
        <a href="https://www.5minutes.one/about">About Us</a>|
        <a href="https://www.5minutes.one/contact">Contact Us</a> 
    </div>    
</div>

<p class="content" style="font-size: 8px;">Copyright (c) 2019 5minutes.</p>
</body>
