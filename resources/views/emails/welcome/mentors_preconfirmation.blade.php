<head>
<!-- Styles -->
<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .row {
                width: 100%;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 32px;
            }

            .links > a {
                color: #FFF;
                padding: 0 8px;
                font-size: 12px;
                /* font-weight: 600; */
                /* letter-spacing: .1rem; */
                text-decoration: none;
                /* text-transform: uppercase; */
            }

            .text-white {
                color: #FFF;
            }

            .back-pad {
                background-color: #006400; padding: 10px 10px 10px 10px;
            }
        </style>
    </head>
<body>  
<div class="flex-center back-pad">
    <div class="title content row">
        <a href="https://www.5minutes.one/" class="text-white content"><b>5minutes</b></a>
    </div> 
</div>
<p style="font-size: 16px"><b>Hi {{$user['name']}}</b></p>
<p>
    Thank you for Signing up for 5minutes Mentor-Mentee Platform. We're excited to have you.
</p>
<p>
    Your registration will be adequately processed and you will be contacted in less than a week. 
</p>
<p>
    Pause for a bit... You know, people ask us why the most benefits of our platform are accrued for mentors and their protégés. Our answer is simple! It is just as they've said, the platform is for these two sets... Mentors and Mentees! 
</p>

<p>
    It is our pleasure as well as an honor to provide you with an environment with which to conveniently carryout your activities in grooming great number of followers as you gently convert them in time as fitting for their personal goals and objectives. 
</p>

<p>Have a prolific week.</p>
<p>Yours truly,</p><br>
<p>Isah Raphael <br>
(Founder, 5minutes)
</p>

<div class="flex-center back-pad">
    <div class="content links row">
        <a href="https://www.5minutes.one/auth/login">Login</a>|
        <a href="https://www.5minutes.one/about">About Us</a>|
        <a href="https://www.5minutes.one/contact">Contact Us</a> 
    </div>    
</div>

<p class="content" style="font-size: 8px;">Copyright (c) 2019 5minutes.</p>

</body>
