@extends('layouts.app_front')

@section('content')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-sm-6">
                <create-forum :initial-users="{{ $users }}"></create-forum>
            </div>
            <div class="col-sm-6">
                <forums :initial-forums="{{ $forums }}" :user="{{ $user }}"></forums>
            </div>
        </div>
    </div>

</div>
@endsection
