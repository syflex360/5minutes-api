@include('include.header.head')

<!-- Liquid layout -->
<body>
    {{-- loader for vuejs --}}
	<div id="app">
	<br><br>

	<!-- Main navbar -->	
		@include('include.header.header')	
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		
		<!-- Main content -->
		<div class="content-wrapper">

            @yield('content')

			<!-- Footer -->
			@include('include.header.footer')	
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>