require('./bootstrap');

window.Vue = require('vue');

window.Bus = new Vue();

window.VueRouter=require('vue-router').default;

window.VueAxios=require('vue-axios').default;

window.Axios=require('axios').default;

import VueTimeago from 'vue-timeago';
import ReadMore from 'vue-read-more';

import { store } from './store';


// registering Modules
Vue.use(VueRouter,VueAxios, axios);

Vue.use(VueTimeago, {
    name: 'timeago', // component name, `timeago` by default
    locale: 'en-US',
    locales: {
      // you will need json-loader in webpack 1
      'en-US': require('vue-timeago/locales/en-US.json')
    }
  });

Vue.use(ReadMore);

// import 'vue2-dropzone/dist/vue2Dropzone.css'


import VueQuillEditor from 'vue-quill-editor'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor, /* { default global options } */)

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('InfiniteLoading', require('vue-infinite-loading'));

// registering global components
Vue.component('mentor-suggest', require('./components/MentorSuggest.vue'));
Vue.component('interest-suggest', require('./components/InterestSuggest.vue'));
Vue.component('follow-button', require('./components/FollowButton.vue'));
Vue.component('interest-button', require('./components/InterestButton.vue'));
Vue.component('like', require('./components/Like.vue'));
Vue.component('my-interest', require('./components/MyInterest.vue'));
Vue.component('Bookmark', require('./components/Bookmark.vue'));
Vue.component('my-pined-articles', require('./components/MyPinedArticles.vue'));
Vue.component('mentor-follow-status', require('./components/feed/partials/FollowStatus.vue'));
Vue.component('new-direct-message', require('./components/message/newDM.vue'));
Vue.component('nav-search', require('./components/navSearch.vue'));

// registering components
Vue.component('after-thought', require('./components/AfterThought.vue'));
Vue.component('edit-profile', require('./components/profile/partials/addEditProfile.vue'));
Vue.component('profile', require('./components/profile/index.vue'));
Vue.component('editprofile', require('./components/profile/editProfile.vue'));


//registering component for Direct Message (dm)
Vue.component('message-notifications', require('./components/messageNotifications.vue'));



import App from './components/App'
import Feed from './components/feed/feed'
import Article from './components/feed/article'
import Articles from './components/articles/index'
import createArticle from './components/articles/create'
import profile from './components/profile/index'
import editProfile from './components/profile/editProfile'
import mentors from './components/Mentors'
import interest from './components/Categories'
import chat from './components/chat/index.vue'
import forum from './components/forum/index'
import message from './components/message/index.vue'
import subscription from './components/subscribe/index'
import invoiceList from './components/invoice/index'
import invoice from './components/invoice/invoice'
import invoicePayment from './components/invoice/payment'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/feed',
            name: 'feeds',
            component: Feed
        },         
        {
            path: '/article/:slug',
            name: 'article',
            props: true,
            component: Article,
        },      
        {
            path: '/5/:slug',
            name: 'profile',
            props: true,
            component: profile
        },
        {
            path: '/edit/profile',
            name: 'editProfile',
            component: editProfile
        },
        {
            path: '/mentors',
            name: 'mentors',
            component: mentors
        },
        {
            path: '/interest',
            name: 'interest',
            component: interest
        },
        {
            path: '/chat',
            name: 'chat',
            component: chat
        }, 
        {
            path: '/articles',
            name: 'articles',
            component: Articles
        },  
        {
            path: '/create',
            name: 'createArticle',
            component: createArticle
        },            
        {
            path: '/forum',
            name: 'forum',
            component: forum
        }, 
        {
            path: '/message',
            name: 'message',
            component: message
        },
        {
            path: '/subscribe/:slug',
            name: 'subscription',
            props: true,
            component: subscription
        }, 
        {
            path: '/invoice/',
            name: 'invoiceList',
            component: invoiceList
        }, 
        {
            path: '/invoice/:TransID',
            name: 'invoice',
            props: true,
            component: invoice
        }, 
        {
            path: '/invoice/payment',
            name: 'invoicePayment',
            props: true,
            component: invoicePayment
        }, 
             
    ],
});


const app = new Vue({
    
    el: '#app',
    store: store,
    components: { App },
    router, 
    // Add this to Vue instance
   
    mounted(){
        this.$store.dispatch('fetch_mentors');
        this.$store.dispatch('fetch_user');
        this.$store.dispatch('getPosts');
        this.$store.dispatch('fetch_unread_message');
    },
});

