import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';


Vue.use(Vuex);

export const store = new Vuex.Store({

    state: {
        posts: [],        
        mentors: [],
        user: [],
        unread_message: [],    
    },

    mutations: {   
        FETCH_POST(state, payLoad) {
            state.posts.unshift(payLoad);
        },          
        updateComments(state, payLoad) {
            let post = state.posts.find((post) => {
                return payLoad.post_id == post.id;
            });
            post.comments = payLoad['comments'];
        },        
        FETCH_MENTORS(state, mentors) {
            state.mentors = mentors;
        },
        FETCH_USER(state, user) {
            state.user = user;
        },        
        FETCH_UNREAD_MESSAGE(state, unread_message) {
            state.unread_message = unread_message;
        },
        
    },

    actions: {        
        getPosts({ commit }){
            axios.get('/api/post').then(response => {
                if(!response.data.error){
                    response.data.data.forEach((post) => {
                        commit('FETCH_POST',post);
                    });
                }
            });
        },
        fetch_mentors({ commit }) {
            return axios.get('/api/mentors')
                .then(response => commit('FETCH_MENTORS', response.data))
                .catch();
        }, 
        fetch_user({ commit }) {
            return axios.get('/api/user')
                .then(response => commit('FETCH_USER', response.data))
                .catch();
        },          
        fetch_unread_message({ commit }) {
            return axios.get('/api/directMessage', {                
                params: {
                    type: 'unread message',
                }
            })
                .then(response => commit('FETCH_UNREAD_MESSAGE', response.data))
                .catch();
        },       
    },

    getters: {
        getPostById: state => (slug) => state.posts.filter(post => post.slug == slug),
        getPostByUserId: state => (user_id) => state.posts.filter(post => post.user_id == user_id),
        getMessage: state => (user_id) => state.message.filter(msg => msg.user_id == 4),
    },

    
});
