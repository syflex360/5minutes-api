<?php
use App\User;
use App\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('log', function(){
  Log::info('This is info log');
  dd('hello log');
});

Route::get('/auth/social/{social}', 'SocialLoginController@redirectToSocial');
Route::get('/auth/{social}/callback', 'SocialLoginController@handleSocialCallback');


Route::get('index', function(){ 
   $data = User::addAllToIndex();
   $data = Post::addAllToIndex();
   dd($data);
});

Route::get('users', function(){
  $data = User::putMapping($ignoreConflicts = true);
  dd($data);
});

Route::get('posts', function(){
    $data = Post::putMapping($ignoreConflicts = true);
//    Post::deleteMapping($ignoreConflicts = true);
    // Article::addAllToIndex();
//    $data = Post::deleteIndex();
    dd($data);
});



Route::get('/search', function() {
  $user = User::searchByQuery(['match' => ['name' => 'Simon']]);
  return $user;
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', function () {
    return view('profile');
});

Route::get('user/id', function () {
  return 4;
});

Route::get('edit/profile', function () {
    return view('app');
});

Route::get('markUnread', function (){
  auth()->user()->unreadNotifications->markAsRead();
  return back();
});

Route::post('testQ', 'PostController@mail');

//  Route::get('/blog/{any}', 'HomeController@blog')->where('any', '.*');

Route::get('/', function(){
  return view('index');
})->name('index');

Route::middleware('auth')->group(function () {
    Route::get('/article/{slug}', function(){
      return view('app');
    })->name('article');

    Route::middleware('auth')->get('/feed', function(){
      return view('app');
    })->name('feed');

    //profile route
    Route::get('5/{slug}', function(){
      return view('app');
    })->name('profile');

    Route::get('/articles', function(){
      return view('app');
    })->name('articles');


    //After Thoughts Route 
    Route::get('/afterThought', 'AfterThoughtController@index')->name('afterThought');
    Route::get('/create/afterThought', 'PostController@create')->name('create.afterThought');
    Route::POST('/afterThought', 'AfterThoughtController@store')->name('store.afterThought');

    //Category form 
    Route::get('/category', 'CategoryController@index')->name('category');
    Route::get('/create/category', 'CategoryController@create')->name('create.category');
    Route::POST('/store/category', 'PostController@store')->name('store.category');

    Route::get('add-to-log', 'HomeController@myTestAddToLog');
    Route::get('logActivity', 'HomeController@logActivity');


    Route::get('/chat', function () {
      return view('app');
    })->name('chat');

    Route::get('/forum', function(){
      return view('app');
    })->name('forum');

    Route::get('/message', function(){
      return view('app');
    })->name('message');

    //Admin routes
    Route::get('admin_register_show', 'AdminAuth\RegisterController@showRegistrationForm');
    Route::post('admin_register', 'AdminAuth\RegisterController@register');

    //Mentors and Interest  to Follow + Suggestion Routes
    Route::get('mentors', function(){
      return view('app');
    })->name('mentors');

    Route::get('interest',  function(){
      return view('app');
    })->name('interest');


    Route::get('subscribe/{slug}',  function(){
      return view('app');
    })->name('subscription');


    Route::get('invoice',  function(){
      return view('app');
    })->name('invoice');

    Route::get('invoice/{TransID}',  function(){
      return view('app');
    })->name('invoice.transId');

    Route::get('invoice/payment',  function(){
      return view('app');
    })->name('invoice.payment');


});





Route::get('/admin_home', function(){
  return view('admin.home');
});

