<?php

use Illuminate\Http\Request;
use App\Channel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {

    Route::resource('test', 'testController');
    Route::resource('country', 'CountryController');
    Route::resource('/suggest/interest', 'CategorySuggestController');
    Route::resource('/contact', 'ContactController'); 
    
    Route::post('login', 'Auth\UserController@login');
    Route::post('register', 'Auth\UserController@register');

    Route::get('social/{social}', 'Auth\SocialLoginController@redirectToSocial');
    Route::get('{social}/callback', 'Auth\SocialLoginController@handleSocialCallback');

    Route::get('signup/activate/{token}', 'Auth\UserController@signupActivate');
    Route::get('signup/resendActivation/{email}', 'Auth\UserController@resendActivation');
    
    //password reset routes
    Route::group(['middleware' => 'api','prefix' => 'password'], function () {    
        Route::post('create', 'Auth\PasswordResetController@create');
        Route::get('find/{token}', 'Auth\PasswordResetController@find');
        Route::post('reset', 'Auth\PasswordResetController@reset');
    });

    Route::resource('categories/', 'CategoryController');
    Route::get('categories/count/articles/{id}', 'CategoryController@countArticles');
    Route::get('categories/count/mentors/{id}', 'CategoryController@countMentors');    
    Route::get('categories/post/{name}', 'PostController@categoryPost');

    Route::resource('mentor', 'MentorController');


    Route::middleware('auth:api')->group(function () {

        Route::resource('/upload', 'UserController');
        
        //authentication 
        Route::get('token/user', 'Auth\UserController@user');
        Route::get('token/logout', 'Auth\UserController@logout');

        Route::resource('/user', 'UserController');
        Route::post('/user/become_referrer', 'UserController@become_referrer');
        Route::post('/user/upload_avatar', 'UserController@upload_avatar');

        Route::resource('post','PostController');
        Route::get('post/inserted/{post_id}', 'PostController@get_inserted_post');

        Route::get('/get/profile/{slug}', 'UserController@getProfile');
        Route::post('update/profile', 'UserController@updateProfile');
        
        Route::get('/interest', 'InterestController@index');       
        
        Route::get('/post/p/{id}', 'PostController@show');
        Route::resource('article', 'ArticleController');
        Route::resource('comment', 'CommentController');
        
        Route::get('/get/profile/post/{slug}', 'PostController@profilePost');

        Route::post('/post/views/', 'PostController@postViews');
    

        Route::resource('/afterThought', 'AfterThoughtController');
        
        Route::resource('/directMessage', 'DirectMessageController');

        Route::resource('/achievement', 'AchievementController');

        Route::get('/toggleFollow', 'FollowController@toggleFollow');
        Route::get('/following', 'FollowController@following'); 


        Route::get('/followings/{slug}', 'FollowController@followings');
        Route::get('/followers/{slug}', 'FollowController@followers');
        
        //forum route
        Route::resource('/forum', 'ForumController');
        Route::resource('/forum_conversation', 'ForumConversationController');
        Route::resource('/forum_users', 'ForumUsersController');

        //message controller
        Route::resource('/message', 'MessageController');

        //chat controller
        Route::resource('/chat', 'ChatController');

        //invoice
        Route::resource('/invoice', 'InvoiceController');
        //transactions
        Route::resource('/transactions', 'TransactionsController');
        //withdrawables
        Route::resource('/withdrawable', 'WithdrawableController');

        //subscriptions categorys
        Route::resource('/subscription/category', 'SubscriptionTypeController');
        
        //subscribe user to exclusive session and mentoring subscriptions
        Route::resource('/ESS/subscription', 'ExclusiveSessionSubscriptionController');
        Route::resource('/MS/subscription', 'SubscriptionController');

        //subscribers or my subscribers
        Route::resource('/subscribers', 'SubscribersController');
        //subscriptions or my subscriptions
        Route::resource('/subscription', 'SubscriptionController');

        //chat controller
        Route::resource('/search', 'SearchController');

        //Note controller
        Route::resource('/note', 'NoteController');

         //notifiications controller (Message and notification)
        Route::resource('/notification', 'NotificationController');

        //media files
        Route::resource('/media', 'PostMediaController');

        //media files
        Route::resource('/flag', 'FlagController');

        //payments
        Route::resource('payment', 'MakePaymentController');
        Route::post('payment/validate', 'MakePaymentController@validate_payment');
        Route::post('payment/verify', 'MakePaymentController@verify_payment');
        Route::resource('payment/card', 'UserCardController');

        Route::resource('social', 'SocialProfileController');

        Route::resource('broadcast', 'BroadcastController');

        Route::resource('exclusive/session', 'ExclusiveSessionController');
        Route::resource('exclusive/session/users', 'ExclusiveSessionUserController');
        Route::resource('exclusive/session/post', 'ExclusiveSessionPostController');

        Route::resource('badge', 'BadgeController');
});
});

//password reset routes
Route::group(['middleware' => 'api','prefix' => 'admin'], function () {    
    Route::resource('user', 'Admin\UserController');
});