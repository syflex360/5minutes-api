<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('mentor_id')->nullable();
            $table->string('referrer_id')->nullable();
            $table->string('subscription_id')->nullable();
            $table->string('subscription_type_id')->nullable();
            $table->string('invoice_id')->nullable();
            $table->string('transaction_type')->nullable();
            $table->boolean('secondary')->nullable();
            $table->string('transaction')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('currency')->nullable();
            $table->string('amount_transacted')->nullable()->default('0');
            $table->string('withdrawable_befor')->nullable()->default('0');
            $table->string('amount_due')->nullable()->default('0');
            $table->string('withdrawable_after')->nullable()->default('0');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
