<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('expirymonth')->nullable();
            $table->string('expiryyear')->nullable();
            $table->string('cardBIN')->nullable();
            $table->string('last4digits')->nullable();
            $table->string('brand')->nullable();
            $table->string('embedtoken')->nullable();
            $table->string('shortcode')->nullable();
            $table->string('expiry')->nullable();
            $table->string('life_time_token')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_cards');
    }
}
