<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('phone')->nullable();
            $table->string('avatar')->default('avatar.png');
            $table->string('cover')->default('default');
            $table->string('social_avatar')->nullable();
            $table->text('about')->nullable();
            $table->date('birthday')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->boolean('is_admin')->nullable()->default(false);
            $table->timestamp('last_login')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->unsignedInteger('status')->default(1);
            $table->boolean('is_mentor')->nullable()->default(false);
            $table->boolean('is_organization')->nullable()->default(false);    
            $table->boolean('is_referrer')->nullable()->default(false);     
            $table->string('referrer_code')->nullable();   
            $table->string('referrer')->nullable();   
            $table->date('referrer_start_date')->nullable();   
            $table->date('referrer_end_date')->nullable(); 
            $table->string('currency_symbol')->nullable();   
            $table->string('currency')->nullable();       
            $table->boolean('active')->default(false);
            $table->string('activation_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
