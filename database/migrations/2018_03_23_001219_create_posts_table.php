<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('category_id')->nullable();            
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->longText('content')->nullable();
            $table->string('content_type')->nullable();
            $table->unsignedInteger('status')->nullable()->default(1);
            $table->datetime('published_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->boolean('publish_task_complete')->default(false);
            $table->integer('likes')->nullable()->default('0');
            $table->integer('views')->nullable()->default('0');
            $table->integer('flag')->nullable()->default('0');
            $table->integer('allow_comment')->nullable()->default('0');
            $table->boolean('is_premium')->nullable()->default(0);
            $table->boolean('is_signed')->nullable()->default(0);
            $table->boolean('is_exclusive')->nullable()->default(0);
            $table->unsignedInteger('exclusive_session_id')->nullable();
            $table->string('featured_media')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
