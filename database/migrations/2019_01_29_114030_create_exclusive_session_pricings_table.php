<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExclusiveSessionPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exclusive_session_pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exclusive_session_id');
            $table->string('currency_symbol')->nullable();
            $table->string('currency')->nullable();
            $table->string('amount')->nullable();
            $table->string('charges')->nullable();
            $table->timestamps();
            $table->foreign('exclusive_session_id')->references('id')->on('exclusive_sessions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exclusive_session_pricings');
    }
}
