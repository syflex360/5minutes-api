<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('subscribe_to')->unsigned();
            $table->integer('subscription_type_id')->unsigned()->nullable();
            $table->string('type')->nullable();//MS, ESS, BS
            $table->string('transID')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('currency')->nullable();
            $table->string('amount')->nullable();
            $table->string('charges')->nullable();
            $table->string('flwRef')->nullable();
            $table->string('paymentURL')->nullable();
            $table->smallInteger('status')->unsigned()->default(0);
            $table->string('method')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->text('description')->nullable();//descrips the invoice
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
