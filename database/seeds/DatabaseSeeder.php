<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            InterestTableSeeder::class,
            ForumsTableSeeder::class,
            SubscriptionTypesTableSeeder::class,
            CountriesTableSeeder::class,
            BadgeTableSeeder::class,
        ]);
    }
}
