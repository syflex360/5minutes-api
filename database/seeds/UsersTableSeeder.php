<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => '5minutes',
            'slug' => '5minutes',
            'email' => '5minutes.one@gmail.com',
            'password' => bcrypt('5minutes'),
            'phone' => '2347088886806',
            'avatar' => '5minutes_avatar.png',
            'cover' => 'default',
            'about' => 'The official 5minutes',
            'is_admin' => true,
            'status' => '1',
            'is_mentor' => true,
            'is_organization' => true,
            'active' => true,
            'is_referrer' => true,
            'referrer_code' => '5minutes',
            'referrer' => '',
        ]);
    }
}
