<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscriptions = [
            [
                'name' => 'Basic', 
                'type' => 'Monthly', 
                'price' => '1000',
                'charges' => '115',
                'currency' => 'NGN',
                'currency_symbol' => '₦',
                'description' => 'Basic',
                'planId' => '5M0001',
                'days' => '30'
            ],
            [
                'name' => 'Clasic', 
                'type' => 'Yearly', 
                'price' => '11000',
                'charges' => '265',
                'currency' => 'NGN',
                'currency_symbol' => '₦',
                'description' => 'Yearly subscriptions',
                'planId' => '5M0002',
                'days' => '365'
            ],
            [
                'name' => 'Basic', 
                'type' => 'Monthly', 
                'price' => '3',
                'charges' => '0.35',
                'currency' => 'USD',
                'currency_symbol' => '$',
                'description' => 'Basic',
                'planId' => '5M0003',
                'days' => '30'
            ],
            [
                'name' => 'Clasic', 
                'type' => 'Yearly',
                'price' => '33',
                'charges' => '0.80',
                'currency' => 'USD',
                'currency_symbol' => '$',
                'description' => 'Yearly subscriptions 	',
                'planId' => '5M0004',
                'days' => '365'
            ],
        ];
        DB::table('subscription_types')->insert($subscriptions);
    }
}
