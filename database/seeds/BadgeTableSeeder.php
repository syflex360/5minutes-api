<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BadgeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $badges = [
            // followers bades list
            [
                'name' => 'followers',
                'achievement' => '1000',
                'level' => '1',
                'icon' => 'friend-blue',
                'description' => 'Below 1k followers',
            ],
            [
                'name' => 'followers', 
                'achievement' => '3000',
                'level' => '2',
                'icon' => 'friend-green',
                'description' => 'Between 1k-3k followers',
            ],
            [
                'name' => 'followers', 
                'achievement' => '5000',
                'level' => '3',
                'icon' => 'friend-red',
                'description' => 'Between 3k-5k followers',
            ],
            [
                'name' => 'followers',
                'achievement' => '10000',
                'level' => '4',
                'icon' => 'friend-purple',
                'description' => 'Between 5k-10k followers',
            ],
            [
                'name' => 'followers', 
                'achievement' => '11000',
                'level' => '4',
                'icon' => 'friend-black',
                'description' => 'Above 11k followers',
            ],

            // top writer bades list
            [
                'name' => 'writer',
                'achievement' => '100',
                'level' => '1',
                'icon' => 'writer-blue',
                'description' => 'Below 1h articles',
            ],
            [
                'name' => 'writer',
                'achievement' => '300',
                'level' => '2',
                'icon' => 'writer-green',
                'description' => 'Between 1h-3h articles',
            ],
            [
                'name' => 'writer',
                'achievement' => '500',
                'level' => '3',
                'icon' => 'writer-red',
                'description' => 'Between 3h-5h articles',
            ],
            [
                'name' => 'writer', 
                'achievement' => '750',
                'level' => '4',
                'icon' => 'writer-purple',
                'description' => 'Between 5h-7.5h articles',
            ],
            [
                'name' => 'writer',
                'achievement' => '1000',
                'level' => '4',
                'icon' => 'writer-black',
                'description' => 'Above 1k articles',
            ],

             // loveed bades list
             [
                'name' => 'loved',
                'achievement' => '1000',
                'level' => '1',
                'icon' => 'loved-blue',
                'description' => 'Below 1k followers',
            ],
            [
                'name' => 'loved',
                'achievement' => '10000',
                'level' => '2',
                'icon' => 'loved-green',
                'description' => 'Between 1k-3k followers',
            ],
            [
                'name' => 'loved', 
                'achievement' => '100000',
                'level' => '3',
                'icon' => 'loved-red',
                'description' => 'Between 3k-5k followers',
            ],
            [
                'name' => 'loved', 
                'achievement' => '300000',
                'level' => '4',
                'icon' => 'loved-purple',
                'description' => 'Between 5k-10k followers',
            ],
            [
                'name' => 'loved',
                'achievement' => '500000',
                'level' => '4',
                'icon' => 'loved-black',
                'description' => 'Above 11k followers',
            ],

            
            
        ];
        DB::table('badge_types')->insert($badges);
    }
}
