<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ForumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forums')->insert([
            'name' => '5minutes',
            'user_id' => 1,
            'description' => '5minutes premium help/support',
            'avatar' => '5minutes_avatar.png',
        ]);
    }
}
