<?php
/*
 * Set specific configuration variables here
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    | Avatar use Intervention Image library to process image.
    | Meanwhile, Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */
    'driver'    => 'gd',

    // Initial generator class
    'generator' => \Laravolt\Avatar\Generator\DefaultGenerator::class,

    // Whether all characters supplied must be replaced with their closest ASCII counterparts
    'ascii'    => false,

    // Image shape: circle or square
    'shape' => 'square',

    // Image width, in pixel
    'width'    => 200,

    // Image height, in pixel
    'height'   => 200,

    // Number of characters used as initials. If name consists of single word, the first N character will be used
    'chars'    => 2,

    // font size
    'fontSize' => 180,

    // convert initial letter in uppercase
    'uppercase' => true,

    // Fonts used to render text.
    // If contains more than one fonts, randomly selected based on name supplied
    'fonts'    => [__DIR__.'/../fonts/OpenSans-Bold.ttf', __DIR__.'/../fonts/rockwell.ttf'],

    // List of foreground colors to be used, randomly selected based on name supplied
    'foregrounds'   => [
        '#FFFFFF',
    ],

    // List of background colors to be used, randomly selected based on name supplied
    'backgrounds'   => [
        '#133337',
        '#333333',
        '#002d48',
        '#988e7a',
        '#7a8498',
        '#331956',
        '#410c37',
        '#0b171d',
        '#009688',
        '#062303',
        '#1b2303',
        '#312303',
        '#402303',
        '#502303',
        '#1b0d45',
        '#1b0d2d',
        '#1b0d1c',
        '#363636',
        '#433d40',
        '#60615f',
        '#484936',
        '#767676',
        '#242123',
        '#1B181A',
        '#363134',
        '#2D292C'
        // '#f44336',
        // '#E91E63',
        // '#9C27B0',
        // '#673AB7',
        // '#3F51B5',
        // '#2196F3',
        // '#03A9F4',
        // '#00BCD4',
        // '#009688',
        // '#4CAF50',
        // '#8BC34A',
        // '#CDDC39',
        // '#FFC107',
        // '#FF9800',
        // '#FF5722',
    ],

    'border'    => [
        'size'  => 0,

        // border color, available value are:
        // 'foreground' (same as foreground color)
        // 'background' (same as background color)
        // or any valid hex ('#aabbcc')
        'color' => 'foreground',
    ],
];
